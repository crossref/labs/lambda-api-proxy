#!/bin/bash
aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 860842417165.dkr.ecr.eu-west-1.amazonaws.com
docker build -t fargate-labs-proxy .
docker tag fargate-labs-proxy:latest 860842417165.dkr.ecr.eu-west-1.amazonaws.com/fargate-labs-proxy:latest
docker push 860842417165.dkr.ecr.eu-west-1.amazonaws.com/fargate-labs-proxy:latest
aws ecs update-service --force-new-deployment --cluster labs-api-cluster --service labs-dev-labs-api-ecs