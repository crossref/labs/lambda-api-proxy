# http://www.graphviz.org/content/cluster

digraph G {
    rankdir = LR;
    nodesep=0.8;
    ranksep=0.5;
    layout = dot;
    
 graph  [nodesep=1, ranksep=1];

    subgraph cluster_0 {
        colorscheme=set39;
        color=1;
        style="";
        penwidth=10;
        fontsize=20;
        node [colorscheme=set39, style="filled", fillcolor=1]
        New [label = "New-Format XML"; shape = "parallelogram"; ];
        Transformed [label = "Transformed XML"; shape = "parallelogram"; ];
        Extract  [label = "Extract New Fields"; shape = "rect"; ];
        
        New -> Extract -> Transformed;
        
        
        label = "Deposit";
        fontsize = 20;
    }
    
    subgraph cluster_1 {
        colorscheme=set39;
        color=5;
        style="";
        penwidth=10;
        fontsize=20;
        node [colorscheme=set39, style="filled", fillcolor=5]
        Proxy [ label = "REST Proxy Endpoint"; shape = "rect"; ];
        Original  [ label = "Live XML"; shape = "parallelogram"; ];
        Annotation [ label = "Annotation Injection"; shape = "rect"; ];
        Retrieve [ label = "Retrieve from Database"; shape = "rect"; ];
        Reinject [ label = "Reinject Data from New Schema"; shape = "rect"; ];
        Final  [ label = "Final XML"; shape = "parallelogram"; ];
        Original -> Annotation -> Retrieve -> Reinject -> Final;
        label = "REST Proxy";
        fontsize = 20;
    }
  
    subgraph cluster_2 {
        colorscheme=set39;
        color=6;
        style="";
        penwidth=10;
        fontsize=20;
        node [colorscheme=set39, style="filled", fillcolor=6]
        Database [ label = "Datastore"; shape="house"; ]
        Extract -> Database;
        label = "Database"
    }
    
    colorscheme=set39;
    color=7;
    style="";
    penwidth=10;
    fontsize=20;
    node [colorscheme=set39, style="filled", fillcolor=7]
    User [shape=oval];
    User -> New;
    User -> Proxy;
    
    
    Transformed -> Live;
    Proxy -> Live;
    Live -> Original;
        
    
    Database -> Retrieve;
    Final -> User;
    
    
    Live [shape=house; label = "Live API"];
}
