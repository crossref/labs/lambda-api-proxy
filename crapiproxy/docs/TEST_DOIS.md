# Crossref Labs Deposit Proxy, Live DOIs, and Static Delivery
The majority of testing and development work for the Crossref Labs Deposit Proxy was conducted using the test API and mocked JSON responses from the static_delivery system.

However, it is also necessary, on occasion, to test the system using live DOIs. As DOIs are permanent and non-erasable (a DOI is for life not just for Christmas™), we use the following DOIs for testing purposes:

* https://doi.org/10.5555/GRFG-ENGF (for journal.article5.4.d0 schema)
* https://doi.org/10.5555/ZBOL-QVPX (for container in journal.article5.4.d0.ror schema) 
* https://doi.org/10.5555/WREV-ELNA (for journal.article5.4.d0.ror schema)
* https://doi.org/10.5555/FGNE-GERX (for journal.article5.4.d0.ror schema)

The metadata entries for these DOIs clearly indicate that they are for testing purposes only.

The system also uses a static delivery system to serve some JSON files. This allows us to simulate a return value from the Live API so that we can build new schema modifications. For example, a request to the Labs API of 10.5555/n0HRokm will result in the contents of src/plugins/static_delivery/DOI/307b5a6be483fbcaae5daffe4fb02730.json being served to the user. Current static-delivery DOIs:

* https://doi.org/10.5555/n0HRokm
