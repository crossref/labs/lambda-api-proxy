# setup nested asyncio
try:
    import nest_asyncio

    nest_asyncio.apply()
except RuntimeError:
    print("Error initializing nest_asyncio")
