import asyncio
import sys
from unittest import TestCase
from unittest.mock import patch, MagicMock, Mock

from moto import mock_s3, mock_cloudwatch, mock_logs

sys.path.append("../../")
sys.path.append("../src")
sys.path.append("../src/plugins")

from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


@patch("whatismyip.whatismyipv4", return_value="192.168.1.1")
@patch("whatismyip.whatismyipv6", return_value="2001:db8::1")
def test_show_stats(mock_ipv4, mock_ipv6):
    response = client.get("/stats/")
    assert response.status_code == 200

    data = response.json()
    assert "ipv4" in data
    assert "ipv6" in data

    ipv4 = data["ipv4"]
    ipv6 = data["ipv6"]

    assert isinstance(ipv4, str) and ipv4 == "192.168.1.1"
    assert isinstance(ipv6, str) and ipv6 == "2001:db8::1"


@mock_s3
@mock_cloudwatch
@mock_logs
class TestShowReports(TestCase):
    def setUp(self):
        self.request = MagicMock()
        self.instrumentation = MagicMock()

    @patch("routers.stats.settings")
    def test_show_reports(self, settings):
        settings.REPORTS_PATH = "reports"
        settings.LABS_PREFIX = "lab_"
        settings.BUCKET = "test-bucket"

        file_list = ["report1.json", "report2.json"]
        file_contents = [
            '{"title": "Report 1", "content": "This is report 1."}',
            '{"title": "Report 2", "content": "This is report 2."}',
        ]

        self.instrumentation.aws_connector.list_prefix = Mock(
            return_value=file_list
        )
        self.instrumentation.aws_connector.s3_obj_to_str = Mock(
            side_effect=file_contents
        )

        expected_result = {
            "status": "ok",
            "message-type": "report",
            "message-version": "1.0.0",
            "message": {
                "lab_report1": {
                    "title": "Report 1",
                    "content": "This is report 1.",
                },
                "lab_report2": {
                    "title": "Report 2",
                    "content": "This is report 2.",
                },
            },
        }

        def run_test():
            from routers.stats import show_reports

            result = show_reports(
                request=self.request, instrumentation=self.instrumentation
            )
            self.assertEqual(result, expected_result)

        run_test()
