from fastapi_proxy import LabsFastAPIProxy


class PluginB:
    def __init__(self, settings, aws_connector, instrumentation):
        self._instrumentation = instrumentation

    @property
    def instrumentation(self):
        """
        The current instrumentation object.
        :return: an Instrumentation object
        """
        return self._instrumentation

    @property
    def routes(self) -> list[str]:
        """
        The list of routes
        :return: the list of routes
        """
        return [r"/test/\d+"]

    def run(
        self,
        response_headers: dict,
        response_json: dict,
        route: str,
        identifier_key: str,
        request_headers: dict = None,
        request_querystring: dict = None,
        key_status: int = 99,
        proxy: LabsFastAPIProxy = None,
    ):
        response_headers["X-Plugin-B"] = "enabled"
        response_json["plugin_b"] = True
        return response_headers, response_json
