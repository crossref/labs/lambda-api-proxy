import json
import sys
import unittest
from unittest.mock import patch, MagicMock, Mock

from moto import mock_s3
from requests import RequestException


@mock_s3
class TestCrossrefProxyPlugin(unittest.TestCase):
    def setUp(self) -> None:
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")

        from claws import aws_utils

        import settings

        self._aws_connector = aws_utils.AWSConnector(bucket=settings.BUCKET)

        from longsight.instrumentation import Instrumentation

        self._instrumentation = Instrumentation(self._aws_connector)

    def test_run_successful_request(self):
        from plugins.crossrefproxy import CrossrefProxyPlugin

        crossref_proxy_plugin = CrossrefProxyPlugin(
            instrumentation=self._instrumentation
        )

        from fastapi_proxy import LabsFastAPIProxy
        import settings

        resource_name = "members"

        settings.POLITE_ONLY = False
        proxy_object = LabsFastAPIProxy(
            settings=settings,
            request=None,
            identifier_key=settings.IDENTIFIER_KEYS[resource_name],
            aws_connector=None,
            instrumentation=self._instrumentation,
            mailto="martin@eve.gd",
        )

        mock_resource = MagicMock()
        mock_resource.headers = {"Content-Type": "application/json"}
        mock_resource.json.return_value = {"key": "value"}

        import requests

        requests.get = Mock(return_value=mock_resource)

        response_headers, response_json = crossref_proxy_plugin.run(
            response_headers={},
            response_json={},
            route="https://example.com",
            identifier_key="id",
            proxy=proxy_object,
        )

        expected_headers = {"Content-Type": "application/json"}
        expected_json = {"key": "value"}

        self.assertEqual(response_headers, expected_headers)
        self.assertEqual(response_json, expected_json)

    @patch("error.error_message")
    def test_run_failed_request(self, mock_error_message):
        from plugins.crossrefproxy import CrossrefProxyPlugin
        import requests

        crossref_proxy_plugin = CrossrefProxyPlugin(
            instrumentation=self._instrumentation
        )

        requests.get = Mock(
            side_effect=requests.exceptions.RequestException("Request failed")
        )

        mock_error_message.return_value = {
            "status": "500",
            "message-type": "Internal Server Error",
            "message": "Internal Server Error: unable to proxy request (Request failed)",
        }

        response_headers, response_json = crossref_proxy_plugin.run(
            response_headers={},
            response_json={},
            route="https://example.com",
            identifier_key="id",
        )

        expected_headers = {}
        expected_json = {
            "status": "500",
            "message-type": "Internal Server Error",
            "message": "Internal Server Error: unable to proxy request (Request failed)",
        }

        self.assertEqual(response_headers, expected_headers)
        self.assertDictEqual(response_json["body"], expected_json)
