import asyncio
import sys
import unittest
from collections import defaultdict
from unittest.mock import MagicMock, AsyncMock, patch
from xml.etree.ElementTree import Element, tostring, fromstring


class TestVerifyCounts(unittest.TestCase):
    def setUp(self):
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")
        self.loop = asyncio.get_event_loop()

    def test_verify_counts(self):
        countlist = [
            {
                "patch": {
                    "foo": ["item1", "item2"],
                    "bar": "item3",
                },
                "data": {"foo": 2, "bar": 1},
            },
            {
                "patch": {
                    "bar": ["item1", "item2", "item3"],
                    "baz": "item4",
                },
                "data": {"foo": 0, "bar": 3, "baz": 1},
            },
        ]
        remaps = {"foo": "new_foo", "baz": "new_baz"}

        expected = [
            {
                "data": {"bar": 1, "foo": 2},
                "patch": {
                    "bar": "item3",
                    "foo": ["item1", "item2"],
                    "tag_mismatches": ["new_foo"],
                },
            },
            {
                "data": {"bar": 3, "baz": 1, "foo": 0},
                "patch": {
                    "bar": ["item1", "item2", "item3"],
                    "baz": "item4",
                    "tag_mismatches": ["new_baz"],
                },
            },
        ]

        from plugins.depositor_rules.process_name import (
            verify_counts,
        )

        # this function modifies the data in countlist rather than returning a
        # anything
        verify_counts(countlist, remaps)
        actual = countlist

        self.assertEqual(expected, actual)


class TestCountNamespacedElements(unittest.TestCase):
    def setUp(self):
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")

        # This XML tree is used in our tests
        self.tree = fromstring(
            """
            <root xmlns:ns1="http://www.ns1.com" xmlns:ns2="http://www.ns2.com">
                <ns1:element>...</ns1:element>
                <ns1:element>...</ns1:element>
                <ns2:element>...</ns2:element>
            </root>
            """
        )
        self.namespaces = {
            "ns1": "http://www.ns1.com",
            "ns2": "http://www.ns2.com",
        }

    def test_count_namespaced_elements(self):
        from plugins.depositor_rules.process_name import (
            count_namespaced_elements,
        )

        # Test that the function counts the correct number of namespaced elements
        ns_element_list = ["ns1:element", "ns2:element"]
        expected = {"ns1:element": 2, "ns2:element": 1}
        actual = count_namespaced_elements(
            self.tree, ns_element_list, self.namespaces
        )
        self.assertEqual(expected, actual)

    def test_empty_ns_element_list(self):
        from plugins.depositor_rules.process_name import (
            count_namespaced_elements,
        )

        # Test that the function returns an empty dictionary when there are no namespaced elements
        ns_element_list = []
        expected = {}
        actual = count_namespaced_elements(
            self.tree, ns_element_list, self.namespaces
        )
        self.assertEqual(expected, actual)

    def test_nonexistent_elements(self):
        from plugins.depositor_rules.process_name import (
            count_namespaced_elements,
        )

        # Test that the function returns zero counts for non-existent elements
        ns_element_list = ["ns1:nonexistent", "ns2:nonexistent"]
        expected = {"ns1:nonexistent": 0, "ns2:nonexistent": 0}
        actual = count_namespaced_elements(
            self.tree, ns_element_list, self.namespaces
        )
        self.assertEqual(expected, actual)


class TestExtractDoi(unittest.TestCase):
    def setUp(self):
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")
        self.loop = asyncio.get_event_loop()

    def test_extract_doi(self):
        from plugins.depositor_rules.process_name import (
            extract_doi,
        )

        json_response = {"message": {"DOI": "10.1234/test"}}

        # Mocking Annotator
        Annotator = MagicMock()
        Annotator.doi_to_md5.return_value = "437236890e5f374fbad39ca88ab89c78"

        instrumentation = MagicMock()
        instrumentation.logger.info = AsyncMock()

        expected = "437236890e5f374fbad39ca88ab89c78"
        actual = extract_doi(instrumentation, json_response)

        self.assertEqual(expected, actual)


class TestApplyPatches(unittest.TestCase):
    def setUp(self):
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")
        self.loop = asyncio.get_event_loop()

    def test_apply_patches(self):
        from plugins.depositor_rules.process_name import (
            apply_patches,
        )

        changelist = [
            {
                "data": "data1",
                "patch": {},
            },
            {
                "data": "data2",
                "patch": {
                    "names": ["existing1", "existing2"],
                },
            },
        ]

        expected = [
            {
                "data": "data1",
                "patch": {
                    "names": ["data1"],
                },
            },
            {
                "data": "data2",
                "patch": {
                    "names": ["existing1", "existing2", "data2"],
                },
            },
        ]

        apply_patches(changelist)
        self.assertEqual(changelist, expected)


class TestBuildChangelist(unittest.TestCase):
    def setUp(self):
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")
        self.loop = asyncio.get_event_loop()

    def test_build_changelist(self):
        from plugins.depositor_rules.process_name import (
            build_changelist,
        )

        deposit = MagicMock()
        deposit.Depositor.compare_json_xml.return_value = True

        changelist = []
        countlist = []
        include = ["element1", "element2"]
        patches = [
            {
                "element": '{"locator": "<person_name><author_name>Name1</author_name></person_name>", "data": "data1", "element_counts": {"crossref:surname": 1}}'
            }
        ]
        remaps = {"element1": "new_element1", "element2": "new_element2"}
        work = {"author": [{"author_name": "Name1"}]}
        ignores = ["element1"]

        expected_changelist = [
            {"data": "data1", "patch": {"author_name": "Name1"}}
        ]
        expected_countlist = [
            {"data": {"crossref:surname": 1}, "patch": {"author_name": "Name1"}}
        ]

        build_changelist(
            changelist, countlist, include, patches, remaps, work, ignores
        )

        self.assertEqual(changelist, expected_changelist)
        self.assertEqual(countlist, expected_countlist)


class TestLoadPatches(unittest.TestCase):
    def setUp(self):
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")
        self.loop = asyncio.get_event_loop()

    @patch("plugins.deposit.Depositor", new_callable=AsyncMock)
    def test_load_patches(self, mock_depositor):
        from plugins.depositor_rules.process_name import (
            load_patches,
        )

        doi_md5 = "1234567890abcdef1234567890abcdef"
        aws_connector = MagicMock()
        aws_connector.list_prefix.return_value = ["file1.json", "file2.json"]
        aws_connector.get_multiple_s3_objs.return_value = ["data1", "data2"]
        instrumentation = MagicMock()
        json_response = {"message": {"DOI": "10.1234/test"}}

        # Mocking extract_doi
        extract_doi = AsyncMock()
        extract_doi.return_value = doi_md5

        # Mocking deposit.Depositor
        mock_depositor.load_patches.return_value = ["data1", "data2"]

        expected = ["data1", "data2"]
        actual = self.loop.run_until_complete(
            load_patches(aws_connector, instrumentation, json_response)
        )

        self.assertEqual(expected, actual)


class TestProcessWorks(unittest.TestCase):
    def setUp(self):
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")
        self.loop = asyncio.get_event_loop()
        self.aws_connector = AsyncMock()
        self.instrumentation = AsyncMock()
        self.json_response = {"message": {"DOI": "10.1234/test"}}
        self.works = ["work1", "work2", "work3"]

    @patch(
        "plugins.depositor_rules.process_name.load_patches",
        new_callable=AsyncMock,
    )
    @patch(
        "plugins.depositor_rules.process_name.build_changelist",
        new_callable=AsyncMock,
    )
    @patch(
        "plugins.depositor_rules.process_name.apply_patches",
        new_callable=AsyncMock,
    )
    @patch(
        "plugins.depositor_rules.process_name.verify_counts",
        new_callable=AsyncMock,
    )
    def test_process_works(
        self,
        mock_verify_counts,
        mock_apply_patches,
        mock_build_changelist,
        mock_load_patches,
    ):
        from plugins.depositor_rules.process_name import (
            process_works,
        )

        mock_load_patches.return_value = ["patch1", "patch2"]

        process_works(
            self.aws_connector,
            self.instrumentation,
            self.json_response,
            self.works,
        )

        mock_load_patches.assert_called()
        mock_build_changelist.assert_called()
        mock_apply_patches.assert_called()
        mock_verify_counts.assert_called()


class TestRepatch(unittest.TestCase):
    def setUp(self):
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")
        self.loop = asyncio.get_event_loop()
        self.aws_connector = AsyncMock()
        self.instrumentation = AsyncMock()

    @patch(
        "plugins.depositor_rules.process_name.process_works",
        new_callable=AsyncMock,
    )
    def test_repatch(self, mock_process_works):
        from plugins.depositor_rules.process_name import (
            repatch,
        )

        json_response = {"message-type": "work", "message": "test_message"}

        expected = {
            "status": "ok",
            "message-version": "1.0.0",
            "message-type": "work",
            "message": "test_message",
        }

        actual = repatch(
            json_response, self.aws_connector, self.instrumentation
        )

        self.assertEqual(expected, actual)

        json_response = {
            "message-type": "works",
            "message": {"items": ["test_message1", "test_message2"]},
        }

        expected = {
            "status": "ok",
            "message-version": "1.0.0",
            "message-type": "work",
            "message": ["test_message1", "test_message2"],
        }

        actual = repatch(
            json_response, self.aws_connector, self.instrumentation
        )

        self.assertEqual(expected, actual)

        json_response = {"message-type": "other", "message": "test_message"}

        actual = repatch(
            json_response, self.aws_connector, self.instrumentation
        )

        self.assertEqual(json_response, actual)
