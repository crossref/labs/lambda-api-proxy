import os
import sys

import pytest

from pytest_localstack.session import LocalstackSession


class TestAnnotatorIntegrations:
    done_setup = False

    @staticmethod
    def _upload_valid_annotation_fixtures(
        client, bucket: str, fixtures_dir: str, aws_dir: str
    ) -> None:
        fixtures_paths = [
            os.path.join(path, filename)
            for path, _, files in os.walk(fixtures_dir)
            for filename in files
        ]
        for path in fixtures_paths:
            key = f"{aws_dir}/{os.path.relpath(path, fixtures_dir)}"
            client.upload_file(
                Filename=path,
                Bucket=bucket,
                Key=key,
                ExtraArgs={"ACL": "public-read"},
            )

    @pytest.fixture()
    def set_up_s3(self):
        if self.done_setup:
            return

        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")

        from claws import aws_utils
        import settings
        from longsight.instrumentation import Instrumentation

        # first check if localstack is running
        if not os.environ.get("GITLAB_CI"):
            import docker

            client = docker.from_env()

            with LocalstackSession(
                services=["s3"], docker_client=client
            ) as session:
                self._aws_connector = aws_utils.AWSConnector(
                    bucket=settings.BUCKET,
                    endpoint_url=session.endpoint_url("s3"),
                )
                self._instrumentation = Instrumentation(
                    aws_connector=self._aws_connector
                )

                res = self.stage_two_setup()
                yield res
        else:
            # change the endpoint URL to use GitLab CLI version
            self._aws_connector = aws_utils.AWSConnector(
                bucket=settings.BUCKET,
                endpoint_url="http://localstack:4566",
            )

            self._instrumentation = Instrumentation(
                aws_connector=self._aws_connector
            )

            res = self.stage_two_setup()
            yield res

    def stage_two_setup(self):
        client = self._aws_connector.s3_client
        client._request_signer.sign = lambda *args, **kwargs: None

        import settings
        import botocore

        _s3 = self._aws_connector.s3_resource

        try:
            _s3.meta.client.head_bucket(Bucket=settings.BUCKET)
        except botocore.exceptions.ClientError:
            pass
        else:
            return _s3, client

        _s3.create_bucket(Bucket=settings.BUCKET)

        client.put_bucket_policy(
            Bucket=settings.BUCKET,
            Policy='{"Version":"2012-10-17", "Statement":[{"Sid":"AddPerm", '
            '"Effect":"Allow", "Principal": "*", "Action":['
            '"s3:GetObject"], "Resource":["arn:aws:s3:::'
            + settings.BUCKET
            + '/*"]}]}',
        )

        self._upload_valid_annotation_fixtures(
            client=client,
            bucket=settings.BUCKET,
            fixtures_dir="integration_test_fixtures/valid_annotations",
            aws_dir=f"{settings.VALID_ANNOTATIONS}/members/",
        )

        self._upload_valid_annotation_fixtures(
            client=client,
            bucket=settings.BUCKET,
            fixtures_dir="integration_test_fixtures/member-78",
            aws_dir=f"{settings.ANNOTATION_PATH}/members/78",
        )

        self._s3 = _s3
        self.client = client

        self.done_setup = True

        return _s3, client

    def test_list_valid_annotations(self, set_up_s3):
        from plugins.annotator import Annotator
        import settings

        annotator_plugin_instance = Annotator(
            settings, aws_connector=self._aws_connector
        )
        valid_annotations = annotator_plugin_instance.get_valid_annotations(
            "members"
        )

        expected_valid_annotations = []

        for i in range(9):
            expected_valid_annotations.append(f"test-annotation-{i + 1}.json")

        assert valid_annotations == set(expected_valid_annotations)

    def test_generate_annotation_list(self, set_up_s3):
        """
        Test that we generate a list of annotations correctly
        :return: None
        """
        from plugins.annotator import Annotator
        import settings

        annotator_plugin_instance = Annotator(
            settings, aws_connector=self._aws_connector
        )

        items = [
            {"id": "item1"},
            {"id": "item2"},
            {"id": "item3"},
            {"id": "item4"},
        ]

        route = "/members"
        identifier_key = "id"

        annotator_plugin_instance.populate_valid_annotations(
            route=route, valid_annotations=None
        )

        annotation_list = annotator_plugin_instance.generate_annotation_list(
            items, route, identifier_key
        )

        expected_annotation_list = []

        for item in items:
            for i in range(9):
                expected_annotation_list.append(
                    f"annotations/members/{item[identifier_key]}"
                    f"/test-annotation-{i + 1}.json"
                )

        assert set(expected_annotation_list).issubset(set(annotation_list))

    def test_get_annotations(self, set_up_s3):
        """
        Test that we can fetch annotations from s3
        :return: None
        """
        from plugins.annotator import Annotator
        import settings

        annotator_plugin_instance = Annotator(
            settings,
            aws_connector=self._aws_connector,
            instrumentation=self._instrumentation,
        )

        annotator_plugin_instance.populate_valid_annotations(
            route="/members/78", valid_annotations=None
        )

        result = annotator_plugin_instance.get_annotations("/members/78")

        expected_result = {}

        for i in range(9):
            expected_result[
                annotator_plugin_instance._settings.LABS_PREFIX
                + f"test-annotation-{i + 1}"
            ] = {}

        # actual results
        expected_result[
            annotator_plugin_instance._settings.LABS_PREFIX
            + "test-annotation-1"
        ] = {"annotation1": 1}

        expected_result[
            annotator_plugin_instance._settings.LABS_PREFIX
            + "test-annotation-3"
        ] = {"annotation3": 3}

        assert result == expected_result
