import sys
import unittest
from unittest.mock import MagicMock, patch

import botocore
from moto import mock_s3


@mock_s3
class SamplerTestCase(unittest.TestCase):
    def setUp(self):
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")

        self.aws_connector = MagicMock()
        self.settings = MagicMock()
        self.settings.SAMPLES_BUCKET = "test-bucket"
        self.settings.REPRESENTATION_VERSION = "1.0.0"

        from claws import aws_utils

        self._aws_connector = aws_utils.AWSConnector(
            bucket=self.settings.SAMPLES_BUCKET
        )

        self.client = self._aws_connector.s3_client
        self.client._request_signer.sign = lambda *args, **kwargs: None

        from longsight.instrumentation import Instrumentation

        self._instrumentation = Instrumentation(self._aws_connector)

        self.sampler = None

        try:
            self._s3 = self._aws_connector.s3_resource
            self._s3.meta.client.head_bucket(
                Bucket=self.settings.SAMPLES_BUCKET
            )
        except botocore.exceptions.ClientError:
            pass
        else:
            err = "{bucket} should not exist.".format(
                bucket=self.settings.SAMPLES_BUCKET
            )
            raise EnvironmentError(err)

        self._s3.create_bucket(Bucket=self.settings.SAMPLES_BUCKET)

        response = self.client.put_bucket_policy(
            Bucket=self.settings.SAMPLES_BUCKET,
            Policy='{"Version":"2012-10-17", "Statement":[{"Sid":"AddPerm", '
            '"Effect":"Allow", "Principal": "*", "Action":['
            '"s3:GetObject"], "Resource":["arn:aws:s3:::'
            + self.settings.SAMPLES_BUCKET
            + '/*"]}]}',
        )

    def test_extract_date(self):
        from plugins.sampler import Sampler

        import settings

        self.sampler = Sampler(
            settings=settings,
            aws_connector=self._aws_connector,
            instrumentation=self._instrumentation,
        )

        key = "members-works/2023-03-24/samples"
        extracted_date = self.sampler.extract_date(key)
        self.assertEqual(extracted_date, "2023-03-24")

    def test_all_member_samples_available(self):
        from plugins.sampler import Sampler

        import settings

        self.sampler = Sampler(
            settings=settings,
            aws_connector=self.aws_connector,
            instrumentation=self._instrumentation,
        )

        self.aws_connector.bucket.objects.filter.return_value = [
            MagicMock(key="all-works/2023-03-24/sample.jsonl")
        ]
        result = self.sampler.all_member_samples_available()

        self.aws_connector.bucket.objects.filter.assert_called()

        self.assertEqual(result, ["2023-03-24"])

    @patch("claws.aws_utils.AWSConnector.s3_obj_to_str")
    def test_all_member_works_data_points(self, mock_s3_obj_to_str):
        from plugins.sampler import Sampler

        import settings

        self.sampler = Sampler(
            settings=settings,
            aws_connector=self._aws_connector,
            instrumentation=self._instrumentation,
        )

        mock_s3_obj_to_str.return_value = (
            '{"data-point": 42}\n{"data-point": 43}\n'
        )
        result = self.sampler.all_member_works_data_points("2023-03-24")
        mock_s3_obj_to_str.assert_called_once()
        self.assertEqual(result, [42, 43])

    @patch("claws.aws_utils.AWSConnector.s3_obj_to_str")
    def test_per_member_samples_available(self, mock_s3_obj_to_str):
        from plugins.sampler import Sampler

        import settings

        self.sampler = Sampler(
            settings=settings,
            aws_connector=self._aws_connector,
            instrumentation=self._instrumentation,
        )

        mock_s3_obj_to_str.return_value = (
            "members-works/2023-03-24/samples/member-1.jsonl\n"
        )
        result = self.sampler.per_member_samples_available(1)
        self.assertEqual(result, ["2023-03-24"])

    @patch("claws.aws_utils.AWSConnector.s3_obj_to_str")
    def test_per_member_works_data_points(self, mock_s3_obj_to_str):
        from plugins.sampler import Sampler

        import settings

        self.sampler = Sampler(
            settings=settings,
            aws_connector=self._aws_connector,
            instrumentation=self._instrumentation,
        )

        mock_s3_obj_to_str.return_value = (
            '{"data-point": 42}\n{"data-point": 43}\n'
        )
        result = self.sampler.per_member_works_data_points(1, "2023-03-24")
        self.assertEqual(result, [42, 43])

    def test_response_header(self):
        from plugins.sampler import Sampler

        import settings

        self.sampler = Sampler(
            settings=settings,
            aws_connector=self._aws_connector,
            instrumentation=self._instrumentation,
        )

        result = self.sampler.response_header(200, "test-message")
        expected = {
            "status": 200,
            "message-type": "test-message",
            "message-version": self.settings.REPRESENTATION_VERSION,
        }
        self.assertEqual(result, expected)

    def test_host_name(self):
        from plugins.sampler import Sampler

        import settings

        self.sampler = Sampler(
            settings=settings,
            aws_connector=self._aws_connector,
            instrumentation=self._instrumentation,
        )

        request = MagicMock()
        request.headers = {
            "x-forwarded-host": "example.com",
            "host": "localhost",
        }
        result = self.sampler.host_name(request)
        self.assertEqual(result, "example.com")

    def test_message(self):
        from plugins.sampler import Sampler

        import settings

        self.sampler = Sampler(
            settings=settings,
            aws_connector=self._aws_connector,
            instrumentation=self._instrumentation,
        )

        items = [1, 2, 3]
        result = self.sampler.message(items)
        expected = {"message": {"data-points-count": 3, "items": items}}
        self.assertEqual(result, expected)
