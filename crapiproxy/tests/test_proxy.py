import unittest
import json
import sys
from unittest.mock import Mock, patch

import pytest
import requests
from requests.structures import CaseInsensitiveDict


class TestProxy(unittest.TestCase):
    def setUp(self) -> None:
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")

        import settings
        from claws import aws_utils

        self._aws_connector = aws_utils.AWSConnector(bucket=settings.BUCKET)

        from longsight.instrumentation import Instrumentation

        self._instrumentation = Instrumentation(self._aws_connector)

    def test_run_plugins(self):
        """
        Test that the plugin runner works
        :return: None
        """
        # define test inputs
        headers = CaseInsensitiveDict({"Content-Type": "application/json"})
        json_data = {"message": "Hello, world!"}
        route_one = "/test"
        route_two = "/test/213"
        identifier_key = "id"

        # set up mock plugin settings
        plugin_settings = {
            "test_plugins.plugin_a": ("plugin_a", "PluginA"),
            "test_plugins.plugin_b": ("plugin_b", "PluginB"),
        }

        # import the function we want to test
        from fastapi_proxy import LabsFastAPIProxy
        import settings

        settings.POLITE_ONLY = False
        proxy_object = LabsFastAPIProxy(
            settings=settings,
            request=DummyRequest(
                headers={}, url="https://api/test", path="/test"
            ),
            identifier_key="test",
            aws_connector=None,
            instrumentation=self._instrumentation,
            mailto="test@test.com",
        )

        # call the function with the mock inputs and settings
        headers, json_data = proxy_object._run_plugins(
            response_headers=headers,
            response_json=json_data,
            route=route_one,
            identifier_key=identifier_key,
            plugin_settings=plugin_settings,
            aws_connector=self._aws_connector,
            key_status=2,
        )

        # assert that the plugins modified the headers and json data as expected
        self.assertIn("X-Plugin-A", headers)
        self.assertNotIn("X-Plugin-B", headers)
        self.assertIn("plugin_a", json_data)
        self.assertNotIn("plugin_b", json_data)
        self.assertEqual(headers["X-Plugin-A"], "enabled")
        self.assertTrue(json_data["plugin_a"])

        # now do it again to test regex handling
        headers = CaseInsensitiveDict({"Content-Type": "application/json"})
        json_data = {"message": "Hello, world!"}

        # call the function with the mock inputs and settings
        headers, json_data = proxy_object._run_plugins(
            response_headers=headers,
            response_json=json_data,
            route=route_two,
            identifier_key=identifier_key,
            plugin_settings=plugin_settings,
            aws_connector=self._aws_connector,
            key_status=2,
        )

        # assert that the plugins modified the headers and json data as expected
        self.assertIn("X-Plugin-A", headers)
        self.assertIn("X-Plugin-B", headers)
        self.assertIn("plugin_a", json_data)
        self.assertIn("plugin_b", json_data)
        self.assertEqual(headers["X-Plugin-A"], "enabled")
        self.assertEqual(headers["X-Plugin-B"], "enabled")
        self.assertTrue(json_data["plugin_a"])
        self.assertTrue(json_data["plugin_b"])

    def test_raise_route_error_status_0(self):
        """
        Test error 0 on route error
        :return: None
        """
        from fastapi_proxy import LabsFastAPIProxy
        import settings
        import plugins.error as error

        settings.POLITE_ONLY = False
        proxy_object = LabsFastAPIProxy(
            settings=settings,
            request=DummyRequest(
                headers={}, url="https://api/members", path="/members"
            ),
            identifier_key="members",
            aws_connector=None,
            instrumentation=self._instrumentation,
            mailto="test@test.com",
        )

        expected_result = {
            "status": "400",
            "message-type": "bad-request",
            "message": "Missing path parameter",
        }
        self.assertDictEqual(
            proxy_object.raise_route_error(
                error.RouteErrorStatus.MISSING_PATH,
                "id",
                route="/members/78",
            )["body"],
            expected_result,
        )

    def test_raise_route_error_status_1(self):
        """
        Test error 1 on route error
        :return: None
        """
        from fastapi_proxy import LabsFastAPIProxy
        import settings
        import plugins.error as error

        settings.POLITE_ONLY = False
        proxy_object = LabsFastAPIProxy(
            settings=settings,
            request=DummyRequest(
                headers={}, url="https://api/members", path="/members"
            ),
            identifier_key="members",
            aws_connector=None,
            instrumentation=self._instrumentation,
            mailto="test@test.com",
        )

        identifier_key = "id"
        expected_result = {
            "status": "400",
            "message-type": "bad-request",
            "message": f"Unable to extract known identifier key "
            f"({identifier_key})",
        }
        self.assertEqual(
            proxy_object.raise_route_error(
                error.RouteErrorStatus.UNKNOWN_IDENTIFIER_KEY,
                identifier_key,
                route="/members/78",
            )["body"],
            expected_result,
        )

    def test_raise_route_error_invalid_status(self):
        """
        Test an invalid error status code
        :return: None
        """
        from fastapi_proxy import LabsFastAPIProxy
        import settings

        settings.POLITE_ONLY = False
        proxy_object = LabsFastAPIProxy(
            settings=settings,
            request=DummyRequest(
                headers={}, url="https://api/members", path="/members"
            ),
            identifier_key="members",
            aws_connector=None,
            instrumentation=self._instrumentation,
            mailto="test@test.com",
        )

        with self.assertRaises(ValueError):
            proxy_object.raise_route_error(99, "id", route="/members/78")

    def test_proxy_api_request_with_error(self):
        """
        Test that the proxy API request handles errors correctly
        :return: None
        """
        from plugins.crossrefproxy import CrossrefProxyPlugin

        proxy_object = CrossrefProxyPlugin(
            instrumentation=self._instrumentation
        )

        # Mock the requests.get function to raise an exception
        requests.get = Mock(
            side_effect=requests.exceptions.RequestException("Error")
        )

        # Call the function with some test data
        path = "/example"
        params = {"key": "value"}
        headers = {"Authorization": "Bearer TOKEN"}

        # Check that the function raises an exception
        with self.assertRaises(requests.exceptions.RequestException):
            proxy_object._proxy_api_request(path, params, headers=headers)

        # Check that the request was made correctly
        requests.get.assert_called_with(
            f"https://api.crossref.org{path}", params=params, headers=headers
        )

    @patch("fastapi_proxy.LabsFastAPIProxy.raise_route_error")
    def test_handler_raise_route_error(
        self,
        mock_raise_route_error,
    ):
        event = {"path": ""}
        context = {}

        from fastapi_proxy import LabsFastAPIProxy
        import settings
        import plugins.error as error

        settings.POLITE_ONLY = False
        proxy_object = LabsFastAPIProxy(
            settings=settings,
            request=DummyRequest(
                headers={}, url="https://api/members", path="/mdfsfsd"
            ),
            identifier_key="mdfsfsd",
            aws_connector=None,
            instrumentation=self._instrumentation,
            mailto=None,
        )

        proxy_object.run()
        mock_raise_route_error.assert_called_with(
            status=error.RouteErrorStatus.UNKNOWN_IDENTIFIER_KEY,
            identifier_key="mdfsfsd",
            route="/mdfsfsd",
        )

    @patch("fastapi_proxy.LabsFastAPIProxy._run_plugins")
    def test_handler_successful_request(self, mock_run_plugins):
        from fastapi_proxy import LabsFastAPIProxy
        import settings

        settings.POLITE_ONLY = False
        proxy_object = LabsFastAPIProxy(
            settings=settings,
            request=DummyRequest(
                headers={}, url="https://api/members", path="/members"
            ),
            identifier_key="members",
            aws_connector=None,
            instrumentation=self._instrumentation,
            mailto="test@test.com",
        )

        event = {}
        context = {}

        mock_run_plugins.return_value = (
            {"Content-Type": "application/json", "key": "value"},
            {"Content-Type": "application/json", "key": "value"},
        )

        expected_response = {"Content-Type": "application/json", "key": "value"}

        response = proxy_object.run()
        self.assertEqual(response, expected_response)

    def test_proxy_api_request(self):
        """
        Test that the proxy API request handles success correctly
        :return: None
        """
        from plugins.crossrefproxy import CrossrefProxyPlugin

        proxy_object = CrossrefProxyPlugin(
            instrumentation=self._instrumentation
        )

        # Mock the requests.get function to return a fake response
        mock_response = Mock(spec=requests.Response)
        mock_response.status_code = 200
        mock_response.json.return_value = {"message": "Hello, world!"}
        requests.get = Mock(return_value=mock_response)

        # Call the function with some test data
        path = "/example"
        params = {"key": "value"}
        headers = {"Authorization": "Bearer TOKEN"}
        response = proxy_object._proxy_api_request(
            path=path, params=params, headers=headers
        )

        # Check that the request was made correctly
        requests.get.assert_called_with(
            f"https://api.crossref.org{path}", params=params, headers=headers
        )

        # Check that the response was returned correctly
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"message": "Hello, world!"})

    def test_instrumentation_property(self):
        test_value = "some_instrumentation_value"

        from fastapi_proxy import LabsFastAPIProxy
        import settings

        event = {
            "path": "/users/123",
            "queryStringParameters": {"name": "John", "age": "30"},
            "headers": {
                "Accept": "application/json",
                "Authorization": "Bearer token",
            },
        }

        proxy_object = LabsFastAPIProxy(
            settings=settings,
            request=DummyRequest(
                headers={}, url="https://api/members", path="/members"
            ),
            identifier_key="members",
            aws_connector=None,
            instrumentation=self._instrumentation,
            mailto="test@test.com",
        )

        proxy_object._instrumentation = test_value
        self.assertEqual(proxy_object._instrumentation, test_value)

    def test_enforce_polite(self):
        from fastapi_proxy import LabsFastAPIProxy
        import settings

        event = {
            "path": "/members/123",
            "queryStringParameters": {"name": "John", "age": "30"},
            "headers": {
                "Accept": "application/json",
                "Authorization": "Bearer token",
            },
        }

        settings.POLITE_ONLY = True
        proxy_object = LabsFastAPIProxy(
            settings=settings,
            request=DummyRequest(
                headers={}, url="https://api/members", path="/members"
            ),
            identifier_key=settings.IDENTIFIER_KEYS["members"],
            aws_connector=None,
            instrumentation=self._instrumentation,
            mailto=None,
        )

        result = proxy_object.run()

        status = "403"
        message_type = "not-polite"
        message = (
            f"The given request did not specify a polite email "
            f"address. Please see: https://github.com/CrossRef/rest-api-doc#good-manners--more-reliable-service"
        )
        expected_body = {
            "status": status,
            "message-type": message_type,
            "message": message,
        }
        expected_response = {
            "headers": {
                "Content-Type": "application/json",
            },
            "statusCode": status,
            "body": expected_body,
        }

        self.assertDictEqual(result, expected_response)

    def test_error_message(self):
        """
        Test that we can raise an error message
        :return: None
        """
        from plugins.error import error_message

        status = "400"
        message_type = "bad-request"
        message = "Invalid input data"
        route = "/members/78"
        expected_body = {
            "status": status,
            "message-type": message_type,
            "message": message,
        }
        expected_response = {
            "headers": {
                "Content-Type": "application/json",
            },
            "statusCode": status,
            "body": expected_body,
        }
        actual_response = error_message(
            status, message_type, message, route=route
        )
        self.assertEqual(actual_response, expected_response)


@pytest.mark.parametrize(
    "headers, querystring, expected",
    [
        ({"User-Agent": "mailto:example@example.com"}, {}, True),
        (
            {"User-Agent": "Mozilla/5.0"},
            "example@example.com",
            True,
        ),
        ({"User-Agent": "Mozilla/5.0"}, {}, False),
        (
            {"User-Agent": "Mozilla/5.0"},
            "",
            False,
        ),
        (
            {"User-Agent": "mailto:example@example.com"},
            "example@example.com",
            True,
        ),
        (
            {"User-Agent": "mailto:example@example.com"},
            "example@example.com",
            True,
        ),
    ],
)
def test_is_polite(headers, querystring, expected):
    from fastapi_proxy import LabsFastAPIProxy
    import settings

    settings.POLITE_ONLY = False
    proxy_object = LabsFastAPIProxy(
        settings=settings,
        request=DummyRequest(
            headers={}, url="https://api/members", path="/members"
        ),
        identifier_key=settings.IDENTIFIER_KEYS["members"],
        aws_connector=None,
        instrumentation=None,
        mailto=None,
    )

    assert proxy_object._is_polite(headers, querystring) == expected


class DummyRequest:
    def __init__(self, headers, url, path):
        self._headers = headers
        self._url = url
        self._path = path

    @property
    def headers(self):
        return self._headers

    @property
    def url(self):
        return DummyURL(self._path)


class DummyURL:
    def __init__(self, path):
        self._path = path

    @property
    def path(self):
        return self._path
