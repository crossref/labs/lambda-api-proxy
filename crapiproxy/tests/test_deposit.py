import json
import sys
import unittest
from unittest.mock import patch, MagicMock

from lxml import etree


class TestExtractNamespace(unittest.TestCase):
    def setUp(self):
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")

    def test_extract_namespace(self):
        from plugins.deposit import Depositor

        # Test that the function extracts the correct namespace from the XML tree
        ns_map = {"ns": "http://www.example.com/ns"}
        root = etree.Element("{http://www.example.com/ns}root", nsmap=ns_map)
        expected = "http://www.example.com/ns"
        actual = Depositor.extract_namespace(root)
        self.assertEqual(expected, actual)

    def test_no_namespace(self):
        from plugins.deposit import Depositor

        # Test that the function returns an empty string when there's no namespace
        root = etree.Element("root")
        expected = ""
        actual = Depositor.extract_namespace(root)
        self.assertEqual(expected, actual)


class TestParseXML(unittest.TestCase):
    def setUp(self) -> None:
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")

    def test_basic_input(self):
        from plugins.deposit import Depositor

        xml_string = b'<?xml version="1.0" encoding="UTF-8"?><doi_batch_diagnostic status="completed" sp="a-cs1"><record_diagnostic status="Success"><doi>10.5555/a.test.deposit</doi><msg>Successfully updated</msg></record_diagnostic><record_diagnostic status="Warning"><doi>10.5555/a.second.test.deposit</doi><msg>Added with conflict</msg></record_diagnostic></doi_batch_diagnostic>'
        expected = ["10.5555/a.test.deposit", "10.5555/a.second.test.deposit"]
        self.assertEqual(
            Depositor.determine_successful_deposits(xml_string), expected
        )

    def test_no_valid_dois(self):
        from plugins.deposit import Depositor

        xml_string = b'<?xml version="1.0" encoding="UTF-8"?><doi_batch_diagnostic status="completed" sp="a-cs1"><record_diagnostic status="Failed"><doi>10.5555/a.failed.deposit</doi><msg>Failed update</msg></record_diagnostic></doi_batch_diagnostic>'
        expected = []
        self.assertEqual(
            Depositor.determine_successful_deposits(xml_string), expected
        )

    def test_mixed_statuses(self):
        from plugins.deposit import Depositor

        xml_string = b'<?xml version="1.0" encoding="UTF-8"?><doi_batch_diagnostic status="completed" sp="a-cs1"><record_diagnostic status="Success"><doi>10.5555/a.test.deposit</doi><msg>Successfully updated</msg></record_diagnostic><record_diagnostic status="Failed"><doi>10.5555/a.failed.deposit</doi><msg>Failed update</msg></record_diagnostic><record_diagnostic status="Warning"><doi>10.5555/a.second.test.deposit</doi><msg>Added with conflict</msg></record_diagnostic></doi_batch_diagnostic>'
        expected = ["10.5555/a.test.deposit", "10.5555/a.second.test.deposit"]
        self.assertEqual(
            Depositor.determine_successful_deposits(xml_string), expected
        )

    def test_empty_input(self):
        from plugins.deposit import Depositor

        xml_string = b""
        expected = []
        self.assertEqual(
            Depositor.determine_successful_deposits(xml_string), expected
        )

    def test_invalid_xml(self):
        from plugins.deposit import Depositor

        xml_string = b'<doi_batch_diagnostic status="completed" sp="a-cs1"><record_diagnostic status="Success"><doi>10.5555/a.test.deposit</doi><msg>Successfully updated</msg></record_diagnostic><record_diagnostic status="Warning"><doi>10.5555/a.second.test.deposit</doi><msg>Added with conflict</msg></record_diagnostic>'
        expected = ["10.5555/a.test.deposit", "10.5555/a.second.test.deposit"]
        self.assertEqual(
            Depositor.determine_successful_deposits(xml_string), expected
        )


class TestValidateXML(unittest.TestCase):
    def setUp(self) -> None:
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")

    @patch("lxml.etree.XMLSchema")
    @patch("lxml.etree.parse")
    def test_valid_xml(self, mock_parse, mock_xml_schema):
        from plugins.deposit import Depositor
        from lxml import etree

        xsd = "path/to/your/xsd/file.xsd"
        valid_xml = """<your-xml>...</your-xml>"""
        xml_tree = etree.fromstring(valid_xml)

        mock_instance = mock_xml_schema.return_value
        mock_instance.validate.return_value = True
        mock_parse.return_value = MagicMock(spec=etree.ElementTree)

        self.assertTrue(Depositor.validate_xml(xsd, xml_tree))

        mock_parse.assert_called_once_with(xsd)
        mock_xml_schema.assert_called_once_with(mock_parse.return_value)
        mock_instance.validate.assert_called_once_with(xml_tree)

    @patch("lxml.etree.XMLSchema")
    @patch("lxml.etree.parse")
    def test_invalid_xml(self, mock_parse, mock_xml_schema):
        from plugins.deposit import Depositor
        from lxml import etree

        xsd = "path/to/your/xsd/file.xsd"
        invalid_xml = """<invalid-xml>...</invalid-xml>"""
        xml_tree = etree.fromstring(invalid_xml)

        mock_instance = mock_xml_schema.return_value
        mock_instance.validate.return_value = False
        mock_parse.return_value = MagicMock(spec=etree.ElementTree)

        self.assertFalse(Depositor.validate_xml(xsd, xml_tree))

        mock_parse.assert_called_once_with(xsd)
        mock_xml_schema.assert_called_once_with(mock_parse.return_value)
        mock_instance.validate.assert_called_once_with(xml_tree)


class TestCompareJsonXml(unittest.TestCase):
    def setUp(self) -> None:
        sys.path.append("../../")
        sys.path.append("../src")
        sys.path.append("../src/plugins")

    def test_compare_json_xml_equality(self):
        from plugins.deposit import Depositor

        # most basic total equality
        json_block = '{"foo": "bar", "baz": {"a": "1", "b": "2"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a><b>2</b></baz></root>"
        )
        self.assertTrue(Depositor.compare_json_xml(json_block, xml_element))

    def test_compare_json_xml_inequality(self):
        from plugins.deposit import Depositor

        # most basic single element inequality fail
        json_block = '{"foo": "bar", "baz": {"a": "1", "b": "3"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a><b>2</b></baz></root>"
        )
        self.assertFalse(Depositor.compare_json_xml(json_block, xml_element))

    def test_compare_json_xml_extra_element_not_in_JSON(self):
        from plugins.deposit import Depositor

        # XML contains an extra element that is not in the JSON
        json_block = '{"foo": "bar", "baz": {"a": "1"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a><b>2</b></baz></root>"
        )
        self.assertFalse(Depositor.compare_json_xml(json_block, xml_element))

    def test_compare_json_xml_extra_element_not_in_XML(self):
        from plugins.deposit import Depositor

        # JSON contains an extra element that is not in the XML
        json_block = '{"foo": "bar", "baz": {"a": "1", "b": "2", "c": "3"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a><b>2</b></baz></root>"
        )
        self.assertFalse(Depositor.compare_json_xml(json_block, xml_element))

    def test_compare_json_xml_test_include(self):
        from plugins.deposit import Depositor

        # Told to include only foo and baz elements, which are not identical
        json_block = '{"foo": "bar", "baz": {"a": "1", "b": "2", "c": "3"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a><b>2</b></baz></root>"
        )
        include = ["foo", "baz", "a", "b", "c"]
        self.assertFalse(
            Depositor.compare_json_xml(json_block, xml_element, include)
        )

    def test_compare_json_xml_include_equality(self):
        from plugins.deposit import Depositor

        # Told to include only foo and baz elements, which are identical
        json_block = (
            '{"foo": "bar", "baz": {"a": "1", "b": "2", "c": "3"}, "qux": "4"}'
        )
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a><b>2</b><c>3</c></baz><qux>5</qux></root>"
        )
        include = ["foo", "baz"]
        self.assertTrue(
            Depositor.compare_json_xml(json_block, xml_element, include)
        )

    def test_compare_json_xml_sub_element_inequality(self):
        from plugins.deposit import Depositor

        # sub-element has an inequality
        json_block = '{"foo": "bar", "baz": {"a": "1", "b": "2", "c": "3"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a><b>2</b><c>4</c></baz></root>"
        )
        include = ["foo", "baz", "a", "b", "c"]
        self.assertFalse(
            Depositor.compare_json_xml(json_block, xml_element, include)
        )

    def test_compare_json_xml_remap(self):
        from plugins.deposit import Depositor

        # test remap of foo element in JSON to doo element in XML
        json_block = '{"foo": "bar", "baz": {"a": "1", "b": "2"}}'
        xml_element = etree.fromstring(
            "<root><doo>bar</doo><baz><a>1</a><b>2</b></baz></root>"
        )
        remaps = {"foo": "doo"}
        self.assertTrue(
            Depositor.compare_json_xml(json_block, xml_element, remaps=remaps)
        )

    def test_compare_json_xml_remap_sub_elements(self):
        from plugins.deposit import Depositor

        # test remap of sub-elements x and y in the JSON to a and b in the XML
        json_block = '{"foo": "bar", "baz": {"x": "1", "y": "2"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a><b>2</b></baz></root>"
        )
        remaps = {"x": "a", "y": "b"}
        self.assertTrue(
            Depositor.compare_json_xml(json_block, xml_element, remaps=remaps)
        )

    def test_compare_json_xml_optional_include(self):
        from plugins.deposit import Depositor

        # test an optional include.
        # It doesn't matter if "b" is missing in the XML
        json_block = '{"foo": "bar", "baz": {"a": "1", "b": "2"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a></baz></root>"
        )
        optional_include = ["b"]
        self.assertTrue(
            Depositor.compare_json_xml(
                json_block, xml_element, optional_include=optional_include
            )
        )

    def test_compare_json_xml_optional_include_inequality(self):
        from plugins.deposit import Depositor

        # test an optional include.
        # It doesn't matter if "b" is missing in the XML
        # But the value of a should trigger a failure
        json_block = '{"foo": "bar", "baz": {"a": "THIS", "b": "2"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a></baz></root>"
        )
        include = ["foo", "baz", "a"]
        optional_include = ["b"]
        self.assertFalse(
            Depositor.compare_json_xml(
                json_block,
                xml_element,
                include=include,
                optional_include=optional_include,
            )
        )

    def test_compare_json_xml_optional_include_only(self):
        from plugins.deposit import Depositor

        # test just an optional include.
        json_block = '{"foo": "bar", "baz": {"a": "1", "b": "2"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a><b>2</b></baz></root>"
        )
        optional_include = ["b"]
        self.assertTrue(
            Depositor.compare_json_xml(
                json_block, xml_element, optional_include=optional_include
            )
        )

    def test_compare_json_xml_optional_include_only_inequality(self):
        from plugins.deposit import Depositor

        # test just an optional include where the values don't match.
        json_block = '{"foo": "bar", "baz": {"a": "1", "b": "3"}}'
        xml_element = etree.fromstring(
            "<root><foo>bar</foo><baz><a>1</a><b>2</b></baz></root>"
        )
        optional_include = ["b"]
        self.assertFalse(
            Depositor.compare_json_xml(
                json_block, xml_element, optional_include=optional_include
            )
        )

    def test_compare_json_xml_remap_and_optional_include(self):
        from plugins.deposit import Depositor

        # test that we can remap "foo" to "food" in an optional include
        json_block = '{"foo": "bar"}'
        xml_element = etree.fromstring("<root><food>bar</food></root>")
        remaps = {"foo": "food"}
        optional_include = ["foo"]
        self.assertTrue(
            Depositor.compare_json_xml(
                json_block,
                xml_element,
                remaps=remaps,
                optional_include=optional_include,
            )
        )

    def test_compare_json_xml_remap_fail(self):
        from plugins.deposit import Depositor

        # test that a remap of "foo" to "fooa" doesn't work (should be "food")
        # in a mandatory include
        json_block = '{"foo": "bar"}'
        xml_element = etree.fromstring("<root><food>bar</food></root>")
        remaps = {"foo": "fooa"}
        include = ["foo"]
        self.assertFalse(
            Depositor.compare_json_xml(
                json_block,
                xml_element,
                remaps=remaps,
                include=include,
            )
        )

    def test_compare_json_xml_remap_with_include(self):
        from plugins.deposit import Depositor

        # test that a remap of "foo" to "food" works in a mandatory include
        json_block = '{"foo": "bar"}'
        xml_element = etree.fromstring("<root><food>bar</food></root>")
        remaps = {"foo": "food"}
        include = ["foo"]
        self.assertTrue(
            Depositor.compare_json_xml(
                json_block,
                xml_element,
                remaps=remaps,
                include=include,
            )
        )

    def test_compare_json_xml_remap_optional_include_fail(self):
        from plugins.deposit import Depositor

        # test that a remap of "foo" to "food" works in an optional include
        # this should fail because the values are different
        json_block = '{"foo": "bard"}'
        xml_element = etree.fromstring("<root><food>bar</food></root>")
        remaps = {"foo": "food"}
        optional_include = ["foo"]
        self.assertFalse(
            Depositor.compare_json_xml(
                json_block,
                xml_element,
                remaps=remaps,
                optional_include=optional_include,
            )
        )

    def test_compare_json_xml_optional_and_mandatory_include_remaps(self):
        from plugins.deposit import Depositor

        # test that we can remap surname to last_name in a mandatory include
        # test that the optional include of orcid works
        json_block = (
            '{"first_name": "Martin", "surname": "Eve", "orcid": "1234"}'
        )
        xml_element = etree.fromstring(
            "<root><first_name>Martin</first_name><last_name>Eve</last_name><orcid>1234</orcid></root>"
        )
        remaps = {"surname": "last_name"}
        include = ["first_name", "last_name"]
        optional_include = ["orcid"]
        self.assertTrue(
            Depositor.compare_json_xml(
                json_block,
                xml_element,
                remaps=remaps,
                include=include,
                optional_include=optional_include,
            )
        )

    def test_compare_json_xml_mismatched_optional_include(self):
        from plugins.deposit import Depositor

        # test that a mismatched optional include causes failure
        json_block = (
            '{"first_name": "Martin", "surname": "Eve", "orcid": "12345"}'
        )
        xml_element = etree.fromstring(
            "<root><first_name>Martin</first_name><last_name>Eve</last_name><orcid>1234</orcid></root>"
        )
        remaps = {"surname": "last_name"}
        include = ["first_name", "last_name"]
        optional_include = ["orcid"]
        self.assertFalse(
            Depositor.compare_json_xml(
                json_block,
                xml_element,
                remaps=remaps,
                include=include,
                optional_include=optional_include,
            )
        )

    def test_compare_json_xml_absent_optional_include(self):
        from plugins.deposit import Depositor

        # test that the absence of an optional include field that is present
        # in the JSON but not in the XML is fine in a real-world-like example
        json_block = (
            '{"first_name": "Martin", "surname": "Eve", "orcid": "1234"}'
        )
        xml_element = etree.fromstring(
            "<root><first_name>Martin</first_name><last_name>Eve</last_name></root>"
        )
        remaps = {"surname": "last_name"}
        include = ["first_name", "last_name"]
        optional_include = ["orcid"]
        self.assertTrue(
            Depositor.compare_json_xml(
                json_block,
                xml_element,
                remaps=remaps,
                include=include,
                optional_include=optional_include,
            )
        )

    def test_compare_json_xml_absent_optional_include_XML(self):
        from plugins.deposit import Depositor

        # test that the absence of an optional include field in the JSON
        # but that is present in the XML is fine in a real-world-like example
        json_block = '{"first_name": "Martin", "surname": "Eve"}'
        xml_element = etree.fromstring(
            "<root><first_name>Martin</first_name><last_name>Eve</last_name><orcid>1234</orcid></root>"
        )
        remaps = {"surname": "last_name"}
        include = ["first_name", "last_name"]
        optional_include = ["orcid"]
        self.assertTrue(
            Depositor.compare_json_xml(
                json_block,
                xml_element,
                remaps=remaps,
                include=include,
                optional_include=optional_include,
            )
        )

    def test_compare_json_xml_with_namespaces(self):
        from plugins.deposit import Depositor

        json_block = {
            "given": "Kukpi7 Wayne",
            "family": "Christian",
            "sequence": "additional",
            "affiliation": [],
        }
        xml_element = etree.fromstring(
            '<person_name xmlns="http://www.crossref.org/schema/5.4.d0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:jats="http://www.ncbi.nlm.nih.gov/JATS1" xmlns:fr="http://www.crossref.org/fundref.xsd" xmlns:mml="http://www.w3.org/1998/Math/MathML" contributor_role="author" sequence="additional" language="en"><given_name>Tinker</given_name><surname>Feeney</surname><name>Tinker</name><name>Tinker Doofus Goodboy Feeney</name></person_name>'
        )
        remaps = {"given_name": "given", "surname": "family"}
        include = [
            "given",
            "family",
            "affiliation",
            "name",
            "sequence",
            "ORCID",
        ]

        self.assertFalse(
            Depositor.compare_json_xml(
                json_block,
                xml_element,
                remaps=remaps,
                include=include,
            )
        )
