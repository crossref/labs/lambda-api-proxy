import json
import re

from claws import aws_utils
from requests.structures import CaseInsensitiveDict
from starlette.responses import (
    StreamingResponse,
    JSONResponse,
    RedirectResponse,
)

from plugins import error as error


class LabsFastAPIProxy:
    def __init__(
        self,
        settings,
        request,
        identifier_key,
        aws_connector=None,
        instrumentation=None,
        mailto=None,
    ):
        self._settings = settings
        self._request = request
        self._identifier_key = identifier_key
        self._aws_connector = aws_connector
        self._instrumentation = instrumentation
        self._mailto = mailto

    def raise_route_error(
        self, status: error.RouteErrorStatus, identifier_key: str, route: str
    ) -> dict:
        if status == error.RouteErrorStatus.MISSING_PATH:
            return error.error_message(
                status="400",
                message_type="bad-request",
                message="Missing path parameter",
                instrumentation=self._instrumentation,
                route=route,
            )
        elif status == error.RouteErrorStatus.UNKNOWN_IDENTIFIER_KEY:
            return error.error_message(
                status="400",
                message_type="bad-request",
                message=f"Unable to extract known identifier key "
                f"({identifier_key})",
                instrumentation=self._instrumentation,
                route=route,
            )
        elif status == error.RouteErrorStatus.NOT_POLITE:
            return error.error_message(
                status="403",
                message_type="not-polite",
                message="The given request did not specify a polite email "
                "address. Please see: "
                "https://api.crossref.org/swagger-ui/index.html#/docs",
                instrumentation=self._instrumentation,
                route=route,
            )
        else:
            raise ValueError("Invalid status provided")

    def run(self):
        """
        The main event handler/path router
        :return: an AWS Lambda response
        """
        # this is the main path router where requests land
        if self._settings.POLITE_ONLY and not self._is_polite(
            self._request.headers, self._mailto
        ):
            return self.raise_route_error(
                status=error.RouteErrorStatus.NOT_POLITE,
                identifier_key="",
                route=self._request.url.path,
            )

        try:
            self._identifier_key = self._settings.IDENTIFIER_KEYS[
                self._identifier_key
            ]
        except KeyError:
            return self.raise_route_error(
                status=error.RouteErrorStatus.UNKNOWN_IDENTIFIER_KEY,
                identifier_key=self._identifier_key,
                route=self._request.url.path,
            )

        # call the data plugins
        response_headers = CaseInsensitiveDict()
        response_json = dict()

        response_headers, response_json = self._run_plugins(
            response_headers=response_headers,
            response_json=response_json,
            route=self._request.url.path,
            identifier_key=self._identifier_key,
            key_status=2,
            plugin_settings=self._settings.DATA_PLUGINS,
            aws_connector=self._aws_connector,
            request=self._request,
        )

        # if serving an all route (which returns str), do not annotate
        if isinstance(response_json, RedirectResponse) or isinstance(
            response_json, JSONResponse
        ):
            return response_json

        # call the modifier plugins
        response_headers, response_json = self._run_plugins(
            response_headers=response_headers,
            response_json=response_json,
            route=self._request.url.path,
            identifier_key=self._identifier_key,
            key_status=2,
            plugin_settings=self._settings.PLUGINS,
            aws_connector=self._aws_connector,
        )

        return response_json

    @property
    def instrumentation(self):
        return self._instrumentation

    @staticmethod
    def _is_polite(headers, mailto_querystring):
        """
        Determine if the request is polite
        :param headers: the headers
        :param mailto_querystring: the querystring
        :return: True if the request is polite, False otherwise
        """
        if mailto_querystring and not isinstance(mailto_querystring, dict):
            return True

        if (
            headers
            and "User-Agent" in headers
            and re.search(r"\bmailto:", headers["User-Agent"])
        ):
            return True

        return False

    def _run_plugins(
        self,
        response_headers: CaseInsensitiveDict[str],
        response_json: dict,
        route: str,
        identifier_key: str,
        key_status: int,
        plugin_settings: dict = None,
        aws_connector: aws_utils.AWSConnector = None,
        request=None,
    ) -> tuple[CaseInsensitiveDict, dict]:
        """
        Run all plugins to mutate the response
        :param response_headers: the response headers
        :param response_json: the response json
        :param route: the route
        :param identifier_key: the identifier key
        :param plugin_settings: the plugin settings or None to use default
        :param aws_connector: the AWS connector
        :param key_status: the key status
        :return: a tuple of headers and json
        """
        settings = self._settings

        plugin_settings = (
            settings.PLUGINS if not plugin_settings else plugin_settings
        )

        for plugin_module, module_and_class_name in plugin_settings.items():
            # import modules
            try:
                i = __import__(plugin_module)
                i_plugin = getattr(i, module_and_class_name[0])

                # get the class object and instantiate it
                class_object = getattr(i_plugin, module_and_class_name[1])
                plugin = class_object(
                    settings, aws_connector, self._instrumentation
                )

                # determine if the plugin should be run
                run_plugin = False

                # we use a fast-fail system here of first checking if the
                # strings simply start with anything in the route. We then
                # proceed to a regex match if that doesn't work
                if "any" in plugin.routes:
                    run_plugin = True

                if not run_plugin and route.startswith(tuple(plugin.routes)):
                    run_plugin = True
                elif not run_plugin:
                    combined = "(" + ")|(".join(plugin.routes) + ")"

                    if re.match(combined, route):
                        run_plugin = True

                if run_plugin:
                    # run the plugin
                    response_headers, response_json = plugin.run(
                        response_headers=response_headers,
                        response_json=response_json,
                        route=route,
                        identifier_key=identifier_key,
                        key_status=key_status,
                        proxy=self,
                        request_querystring=request.query_params
                        if request
                        else {},
                    )
            except Exception as e:
                import traceback

                self.instrumentation.logger.error(
                    f"Problem loading plugin ({plugin_module}): {e}. "
                    f"{traceback.format_exc()}"
                )

        return response_headers, response_json
