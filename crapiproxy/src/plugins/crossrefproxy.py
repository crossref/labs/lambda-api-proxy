import json
import pathlib
import unittest

from timeit import default_timer as timer

import plugins.error as error
import requests
from aws_lambda_powertools import single_metric
from aws_lambda_powertools.metrics import MetricUnit
from claws import aws_utils
from fastapi import HTTPException
from fastapi_proxy import LabsFastAPIProxy
from longsight.instrumentation import Instrumentation
from requests.structures import CaseInsensitiveDict
from starlette.datastructures import QueryParams
from starlette.responses import (
    RedirectResponse,
    StreamingResponse,
    JSONResponse,
)


class CrossrefProxyPlugin:
    """
    A plugin to proxy requests to the Crossref REST API. Note that this class
    will overwrite any response objects that are passed to it. It does not
    merge its results with any existing response objects.
    """

    def __init__(
        self,
        settings=None,
        aws_connector: aws_utils.AWSConnector = None,
        instrumentation: Instrumentation = None,
    ):
        self._instrumentation = instrumentation
        self._aws_connector = aws_connector
        self._settings = settings

    @property
    def instrumentation(self):
        """
        The current instrumentation object.
        :return: an Instrumentation object
        """
        return self._instrumentation

    @property
    def routes(self) -> list[str]:
        """
        The list of routes
        :return: the list of routes
        """
        return [
            "/funders",
            "/journals",
            "/works",
            "/prefixes",
            "/members",
            "/types",
            "/licenses",
            "/deposits",
        ]

    def _proxy_api_request(
        self, path, params, headers=None, identifier_key=None
    ):
        """
        Proxy an API request to the Crossref API.
        :param path: the path to proxy
        :param params: the querystring parameters to proxy
        :param headers: the headers to proxy
        :return: a requests.Response object
        """

        has_local, contents = self.check_local_delivery(
            path=path, identifier_key=identifier_key
        )

        if has_local:
            return json.loads(contents)

        self._instrumentation.logger.info(
            f"Fetching https://api.crossref.org{path}"
        )

        # strip out "plain" from the querystrings
        params_new = params.copy()
        if "plain" in params_new:
            del params_new["plain"]

        try:
            return requests.get(
                f"https://api.crossref.org{path}",
                params=params_new,
                headers=headers,
            )

        except Exception as e:
            raise e

    def check_local_delivery(self, path, identifier_key):
        """
        Check for a local delivery file and serve it
        :param path: the path to check
        :return: tuple of whether the file exists and the contents
        """
        from plugins.annotator import Annotator

        if identifier_key == "DOI":
            psplit = path.rsplit("/", 3)
            doi = Annotator.doi_to_md5(f"{psplit[-2]}/{psplit[-1]}")
            local_path = f"plugins/static_delivery/{identifier_key}/{doi}.json"
        else:
            local_path = f"plugins/static_delivery/{path[:-1]}.json"

        local_path_object = pathlib.Path(local_path).resolve()

        if (
            pathlib.Path("plugins/static_delivery").resolve()
            not in local_path_object.parents
        ):
            raise Exception("Attempted path traversal exploit")

        if local_path_object.exists():
            self._instrumentation.logger.info(
                f"Using local cache instead of remote fetch"
            )

            return True, local_path_object.read_text()
        else:
            return False, None

    def process_transform(self, route, request_querystring: QueryParams):
        """
        Remove the transform from the route
        :param route: the route to remove the transform from
        :param request_querystring: the querystring for the request
        :return: the route with the transform removed and a boolean of whether this is a transform
        """

        new_request_querystring = (
            request_querystring.__dict__["_dict"] if request_querystring else {}
        )

        if route.endswith("/transform/application/members-all-simple+json"):
            route = route[
                : -len("/transform/application/members-all-simple+json")
            ]
            new_request_querystring["simple"] = True
            new_request_querystring["rows"] = "all"

            return route, True, new_request_querystring

        return route, False, new_request_querystring

    def run(
        self,
        response_headers: dict,
        response_json: dict,
        route: str,
        identifier_key: str,
        request_headers: dict = None,
        request_querystring: QueryParams = None,
        key_status: error.RouteErrorStatus = error.RouteErrorStatus.OK,
        proxy: LabsFastAPIProxy = None,
    ) -> tuple:
        resource = None

        route, transformed, request_querystring = self.process_transform(
            route, request_querystring
        )

        try:
            # check the key status
            if (key_status == 0 or key_status == 1) and proxy:
                return proxy.raise_route_error(
                    status=error.RouteErrorStatus(key_status),
                    identifier_key=identifier_key,
                )

            # return if the cursor is set to the magic value
            if (
                request_querystring
                and "cursor" in request_querystring
                and request_querystring["cursor"] == "CR-LABS-FINAL"
            ):
                response_headers, response_json = self.magic_cursor(
                    response_headers, response_json
                )

                return response_headers, response_json

            # setup routes
            all_routes, stripped_route = self.setup_routes(route)

            # redirect to "all" routes if we can
            if (
                request_querystring
                and "rows" in request_querystring
                and stripped_route in all_routes
            ):
                if request_querystring["rows"] == "all":
                    row_count = 999999
                    count = 1
                else:
                    # fetch the {stripped_route}_count file from S3 that tells
                    # us how many items there are for this route
                    result = json.loads(
                        self._aws_connector.s3_obj_to_str(
                            bucket=self._settings.BUCKET,
                            s3_path=f"api_artifacts/{stripped_route}_count.json",
                        )
                    )

                    try:
                        count = int(result["count"])
                    except ValueError:
                        count = 0

                    try:
                        row_count = int(request_querystring["rows"])
                    except ValueError:
                        row_count = 20

                if row_count >= count:
                    # return "all" response
                    if (
                        "members" in stripped_route
                        and "select" in request_querystring
                        and request_querystring["select"]
                    ):
                        # if the user gives "select=id,primary-title" (or, in
                        # fact anything for "select") then we return the simple
                        # version
                        response_json = self.get_all_contents(
                            stripped_route,
                            simple=True,
                            names=("names" in request_querystring["select"]),
                        )
                    else:
                        response_json = self.get_all_contents(stripped_route)

                else:
                    proxy_time, resource = self.call_proxy(
                        identifier_key,
                        request_headers,
                        request_querystring,
                        route,
                        proxy,
                    )

                    response_headers, response_json = self.set_response(
                        resource, response_headers, response_json
                    )

                response_headers = {"Content-Type": "application/json"}

            else:
                proxy_time, resource = self.call_proxy(
                    identifier_key,
                    request_headers,
                    request_querystring,
                    route,
                    proxy,
                )

                response_headers, response_json = self.set_response(
                    resource, response_headers, response_json
                )

        except requests.exceptions.RequestException as e:
            if resource is not None and resource.status_code == 404:
                return response_headers, error.error_message(
                    status="404",
                    message_type="Not Found",
                    message=f"Resource not found: {e}",
                    route=route,
                )
            else:
                response_json = error.error_message(
                    status="500",
                    message_type="Internal Server Error",
                    message=f"Internal Server Error: unable to proxy request ({e})",
                    route=route,
                )

        return response_headers, response_json

    def set_response(self, resource, response_headers, response_json):
        if isinstance(resource, dict):
            # if we land here, it's because we're serving a local
            # file from static_delivery rather than a proxied request
            response_json = resource
        else:
            response_headers: CaseInsensitiveDict = resource.headers
            response_json = resource.json()
        return response_headers, response_json

    def call_proxy(
        self, identifier_key, request_headers, request_querystring, route, proxy
    ):
        start = timer()
        resource = self._proxy_api_request(
            path=route,
            params=request_querystring,
            headers=request_headers,
            identifier_key=identifier_key,
        )
        end = timer()
        proxy_time = end - start

        if proxy.instrumentation:
            with single_metric(
                name="Proxy API Request Time",
                unit=MetricUnit.Seconds,
                value=proxy_time,
                namespace=proxy.instrumentation.namespace,
            ) as metric:
                metric.add_dimension(name="route", value=route)

        return proxy_time, resource

    def setup_routes(self, route):
        offset_routes = ["types"]
        cursor_routes = ["members", "journals", "funders"]
        all_routes = offset_routes + cursor_routes
        stripped_route = route.strip("/")
        return all_routes, stripped_route

    def magic_cursor(self, response_headers, response_json):
        response_json = {
            "status": "ok",
            "message-type": "unknown",
            "message-version": "1.0.0",
            "message": {"total-results": 0, "items": []},
        }
        response_headers = CaseInsensitiveDict()
        return response_headers, response_json

    def get_all_contents(self, stripped_route, simple=False, names=False):
        try:
            if simple:
                """
                If we get here, it's because te user specified simple=True on
                a members route.
                """

                stripped_route = "members_simple"

                if names:
                    stripped_route += "_with_names"

            presigned_url = (
                self._aws_connector.s3_client.generate_presigned_url(
                    ClientMethod="get_object",
                    Params={
                        "Bucket": self._settings.BUCKET,
                        "Key": f"api_artifacts/{stripped_route}.json",
                    },
                    ExpiresIn=60,
                )
            )

            response = RedirectResponse(url=presigned_url)

            return response
        except Exception as e:
            if hasattr(e, "message"):
                raise HTTPException(
                    status_code=e.message["response"]["Error"]["Code"],
                    detail=e.message["response"]["Error"]["Message"],
                )
            else:
                raise HTTPException(status_code=500, detail=str(e))
