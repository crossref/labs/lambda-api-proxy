import json

from claws import aws_utils

from longsight.instrumentation import Instrumentation


class Sampler:
    def __init__(
        self,
        settings,
        aws_connector: aws_utils.AWSConnector,
        instrumentation: Instrumentation = None,
    ):
        self._aws_connector = aws_connector
        self._settings = settings
        self._instrumentation = instrumentation

    @property
    def instrumentation(self):
        """
        The current instrumentation object.
        :return: an Instrumentation object
        """
        return self._instrumentation

    @staticmethod
    def extract_date(key, index=-2):
        return key.split("/")[index]

    def all_member_samples_available(self):
        return [
            self.extract_date(obj.key, -2)
            for obj in self._aws_connector.bucket.objects.filter(
                Prefix="all-works/20"
            )
            if obj.key.endswith(".jsonl")
        ]

    def all_member_works_data_points(self, iso_date):
        return [
            json.loads(data_point)["data-point"]
            for data_point in self._aws_connector.s3_obj_to_str(
                bucket=self._settings.SAMPLES_BUCKET,
                s3_path=f"all-works/{iso_date}/sample.jsonl",
            ).splitlines()
        ]

    def per_member_samples_available(self, member_id):
        return [
            self.extract_date(data_point, -3)
            for data_point in self._aws_connector.s3_obj_to_str(
                bucket=self._settings.SAMPLES_BUCKET,
                s3_path=f"members-works/index/member-{member_id}",
            ).splitlines()
        ]

    def per_member_works_data_points(self, member_id, iso_date):
        return [
            json.loads(data_point)["data-point"]
            for data_point in self._aws_connector.s3_obj_to_str(
                bucket=self._settings.SAMPLES_BUCKET,
                s3_path=f"members-works/{iso_date}/samples/member-{member_id}.jsonl",
            ).splitlines()
        ]

    def response_header(self, status, message_type):
        return {
            "status": status,
            "message-type": message_type,
            "message-version": self._settings.REPRESENTATION_VERSION,
        }

    @staticmethod
    def host_name(request):
        return request.headers.get(
            "x-forwarded-host", request.headers.get("host")
        )

    @staticmethod
    def message(items):
        return {"message": {"data-points-count": len(items), "items": items}}
