from enum import Enum

from aws_lambda_powertools import single_metric
from aws_lambda_powertools.metrics import MetricUnit
from longsight.instrumentation import Instrumentation


class RouteErrorStatus(Enum):
    MISSING_PATH = 0
    UNKNOWN_IDENTIFIER_KEY = 1
    NOT_POLITE = 2
    OK = 99


def error_message(
    status: str,
    message_type: str,
    message: str,
    route: str,
    instrumentation: Instrumentation = None,
) -> dict:
    """
    Return an error status
    :param status: the status code (e.g. "400")
    :param message_type: the message type (e.g. "bad-request")
    :param message: a human-readable message
    :param instrumentation: an instrumentation object for CloudWatch logging
    :param route: the route that caused the error
    :return: a dictionary containing the error status
    """
    error_body = {
        "status": status,
        "message-type": message_type,
        "message": message,
    }

    if instrumentation:
        with single_metric(
            name="Application Error",
            unit=MetricUnit.Count,
            value=1,
            namespace=instrumentation.namespace,
        ) as metric:
            metric.add_dimension(name="route", value=route)
            metric.add_dimension(name="status", value=status)
            metric.add_dimension(name="message-type", value=message_type)

    return {
        "headers": {
            "Content-Type": "application/json",
        },
        "statusCode": status,
        "body": error_body,
    }
