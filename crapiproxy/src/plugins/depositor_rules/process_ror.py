import json
from collections import defaultdict

from claws.aws_utils import AWSConnector
from longsight.instrumentation import Instrumentation

from plugins import deposit


def process(
    xml, xml_filename, change_xml_namespace_callable, validate_xml_callable
) -> tuple[dict, dict]:
    """
    Process the XML file to extract the names and mutate the XML.
    :param xml: the xml data
    :param xml_filename: the xml filename
    :param change_xml_namespace_callable: a callable to change the XML namespace
    :param validate_xml_callable: a callable that returns a bool to validate the XML
    :return: a dictionary of XML filenames and the mutated XML
    """

    funding_data = {}
    mutated_xml = {}

    from plugins.deposit import Depositor

    crossref_namespace = Depositor.extract_namespace(xml)
    fundref_namespace = Depositor.extract_namespace(
        xml,
        namespace="http://www.crossref.org/fundref.xsd",
        namespace_mode=True,
    )

    namespaces = {
        "crossref": crossref_namespace,
        fundref_namespace: "http://www.crossref.org/fundref.xsd",
    }

    fundref_assertions = xml.xpath(
        f"//{fundref_namespace}:assertion[@name = 'fundgroup']",
        namespaces=namespaces,
    )

    _process_fundref(
        funding_data,
        namespaces,
        fundref_assertions,
        xml=xml,
        xml_file=xml_filename,
        fundref_namespace=fundref_namespace,
    )

    mutated_xml[xml_filename] = change_xml_namespace_callable(
        xml=xml,
        new_schema="http://www.crossref.org/schema/5.3.1",
        version="5.3.1",
        original_ns=crossref_namespace,
    )

    xsd_file = f"plugins/depositor_schema/5.3.1/crossref5.3.1.xsd"

    is_valid = validate_xml_callable(
        xsd_file=xsd_file,
        xml_element_tree=mutated_xml[xml_filename],
        base_url="plugins/depositor_schema/5.3.1/",
        reload=True,
    )

    if not is_valid:
        raise Exception("Transformed XML is not valid")

    return funding_data, mutated_xml


def count_namespaced_elements(tree, ns_element_list, namespaces) -> dict:
    """
    Count the number of namespaced elements in the XML
    :param tree: the tree
    :param ns_element_list: the elements to count
    :param namespaces: the namespace
    :return: a dictionary of counts
    """
    count_dict = defaultdict(int)

    for ns_element in ns_element_list:
        ns, element = ns_element.split(":")
        count_dict[ns_element] += len(
            tree.findall(f".//{ns}:{element}", namespaces=namespaces)
        )

    return dict(count_dict)


def repatch(
    json_response: dict,
    aws_connector: AWSConnector,
    instrumentation: Instrumentation = None,
):
    """
    Repatch the JSON response with the previously stored names data
    :param json_response: the JSON response
    :param aws_connector: a CLAWS AWS Connector object
    :param instrumentation: a longsight instrumentation object
    :return: a JSON response
    """
    if instrumentation:
        instrumentation.logger.info("Repatching...")

    result_template = {
        "status": "ok",
        "message-version": "1.0.0",
    }

    result_template["message-type"] = "work"
    result_template["message"] = ""
    return result_template


def _process_fundref(
    funding_data,
    namespaces,
    fundref_assertions,
    xml,
    xml_file,
    fundref_namespace,
):
    """
    Process the person names
    :param funding_data: the names data
    :param namespaces: a namespace mapping
    :param fundref_assertions: the extracted fundref blocks
    :param xml: the xml
    :param xml_file: the xml filename
    :return: None (inputs are mutated in place)
    """
    for fundref_assertion in fundref_assertions:
        doi, grant_dois, rors = _extract_elements(
            namespaces, fundref_assertion, fundref_namespace
        )

        # build a dictionary with the count of these elements: crossref:name,
        # crossref:surname, crossref:given_name

        elements_count = count_namespaced_elements(
            tree=fundref_assertion,
            ns_element_list=[
                f"{fundref_namespace}:assertion[@name='ror']",
                f"{fundref_namespace}:assertion[@name='grant_doi']",
            ],
            namespaces=namespaces,
        )

        merged_list = grant_dois + rors

        _process_assertions(
            doi,
            elements_count,
            merged_list,
            funding_data,
            namespaces,
            fundref_assertion,
            xml_file,
        )


def _extract_elements(namespaces, fundref_assertion, fundref_namespace):
    """
    Extract the "name" elements from the XML
    :param namespaces: the namespaces mappings
    :param fundref_assertion:
    :return: doi, grant_dois, and ror XML assertion elements
    """
    # important note: there is no test case for this method as it is very hard
    # to mock LXML objects, which are implemented in C for performance reasons
    # the fix is to restructure the method to use dependency injection with
    # the relevant callables.

    # TODO: restructure function parameters to use dependency injection

    # get parallel doi_data
    program_element = fundref_assertion.getparent()
    work_element = program_element.getparent()

    if work_element.tag == f"{{{namespaces['crossref']}}}custom_metadata":
        work_element = work_element.getparent()

    if work_element.tag == f"{{{namespaces['crossref']}}}crossmark":
        work_element = work_element.getparent()

    doi_data_element = work_element.find(
        f"crossref:doi_data/crossref:doi", namespaces=namespaces
    )
    doi = doi_data_element.text

    grant_dois = fundref_assertion.findall(
        f"{fundref_namespace}:assertion[@name='grant_doi']",
        namespaces=namespaces,
    )

    rors = fundref_assertion.findall(
        f"{fundref_namespace}:assertion[@name='ror']",
        namespaces=namespaces,
    )

    return doi, grant_dois, rors


def _process_assertions(
    doi,
    elements_count,
    assertions,
    assertions_data,
    namespaces,
    fundgroup,
    xml_file,
):
    """
    Process the "name" fields
    :param doi: the doi
    :param elements_count: the number of elements
    :param assertions: the ROR and grant_doi assertion elements
    :param assertions_data: the actual name data
    :param namespaces: the namespaces
    :param fundgroup: the fundgroup block
    :param xml_file: the xml filename
    :return: None (inputs are mutated in place)
    """
    for asserts in assertions:
        if xml_file not in assertions_data:
            assertions_data[xml_file] = []

        from lxml import etree

        # remove the assertion element from the tree
        fundgroup.remove(asserts)

        # we store the locator last so that the modified version is used
        assertions_data[xml_file].append(
            (asserts.text, etree.tostring(fundgroup), doi, elements_count)
        )
