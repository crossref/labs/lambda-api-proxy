import json
from collections import defaultdict

from claws.aws_utils import AWSConnector
from longsight.instrumentation import Instrumentation

from plugins import deposit


def process(
    xml, xml_filename, change_xml_namespace_callable, validate_xml_callable
) -> tuple[dict, dict]:
    """
    Process the XML file to extract the names and mutate the XML.
    :param xml: the xml data
    :param xml_filename: the xml filename
    :param change_xml_namespace_callable: a callable to change the XML namespace
    :param validate_xml_callable: a callable that returns a bool to validate the XML
    :return: a dictionary of XML filenames and the mutated XML
    """

    names_data = {}
    mutated_xml = {}

    from plugins.deposit import Depositor

    crossref_namespace = Depositor.extract_namespace(xml)

    namespaces = {"crossref": crossref_namespace}

    person_names = xml.xpath(
        f"//crossref:contributors/crossref:person_name[crossref:name]",
        namespaces=namespaces,
    )

    _process_person(
        names_data, namespaces, person_names, xml=xml, xml_file=xml_filename
    )

    mutated_xml[xml_filename] = change_xml_namespace_callable(
        xml=xml,
        new_schema="http://www.crossref.org/schema/5.3.1",
        version="5.3.1",
        original_ns=crossref_namespace,
    )

    xsd_file = f"plugins/depositor_schema/5.3.1/crossref5.3.1.xsd"

    is_valid = validate_xml_callable(
        xsd_file=xsd_file,
        xml_element_tree=mutated_xml[xml_filename],
        base_url="plugins/depositor_schema/5.3.1/",
        reload=True,
    )

    if not is_valid:
        raise Exception("Transformed XML is not valid")

    return names_data, mutated_xml


def count_namespaced_elements(tree, ns_element_list, namespaces) -> dict:
    """
    Count the number of namespaced elements in the XML
    :param tree: the tree
    :param ns_element_list: the elements to count
    :param namespaces: the namespace
    :return: a dictionary of counts
    """
    count_dict = defaultdict(int)

    for ns_element in ns_element_list:
        ns, element = ns_element.split(":")
        count_dict[ns_element] += len(
            tree.findall(f".//{ns}:{element}", namespaces=namespaces)
        )

    return dict(count_dict)


def _process_person(names_data, namespaces, person_names, xml, xml_file):
    """
    Process the person names
    :param names_data: the names data
    :param namespaces: a namespace mapping
    :param person_names: the extracted person_names blocks
    :param xml: the xml
    :param xml_file: the xml filename
    :return: None (inputs are mutated in place)
    """
    for person_name in person_names:
        doi, names = _extract_elements(namespaces, person_name)

        # build a dictionary with the count of these elements: crossref:name,
        # crossref:surname, crossref:given_name

        elements_count = count_namespaced_elements(
            tree=person_name,
            ns_element_list=[
                "crossref:name",
                "crossref:surname",
                "crossref:given_name",
            ],
            namespaces=namespaces,
        )

        _process_names(
            doi,
            elements_count,
            names,
            names_data,
            namespaces,
            person_name,
            xml_file,
        )


def _extract_elements(namespaces, person_name):
    """
    Extract the "name" elements from the XML
    :param namespaces: the namespaces mappings
    :param person_name:
    :return: a person_name XML element
    """
    # important note: there is no test case for this method as it is very hard
    # to mock LXML objects, which are implemented in C for performance reasons
    # the fix is to restructure the method to use dependency injection with
    # the relevant callables.

    # TODO: restructure function parameters to use dependency injection

    # get parallel doi_data
    contributors_element = person_name.getparent()
    work_element = contributors_element.getparent()
    doi_data_element = work_element.find(
        f"crossref:doi_data/crossref:doi", namespaces=namespaces
    )
    doi = doi_data_element.text
    names = person_name.findall(f"crossref:name", namespaces=namespaces)
    return doi, names


def _process_names(
    doi, elements_count, names, names_data, namespaces, person_name, xml_file
):
    """
    Process the "name" fields
    :param doi: the doi
    :param elements_count: the number of elements
    :param names: the name elements
    :param names_data: the actual name data
    :param namespaces: the namespaces
    :param person_name: the person_name block
    :param xml_file: the xml filename
    :return: None (inputs are mutated in place)
    """
    for name in names:
        if xml_file not in names_data:
            names_data[xml_file] = []

        surname = person_name.find("crossref:surname", namespaces=namespaces)

        from lxml import etree

        # note that a "not" check on this LXML object doesn't work
        # it needs the full "is None" check
        if surname is None:
            surname_element = etree.Element(
                etree.QName(namespaces["crossref"], "surname")
            )
            surname_element.text = name.text

            # make sure that the surname element is inserted after
            # a given_name element, if that exists
            given_name = person_name.find(
                "crossref:given_name", namespaces=namespaces
            )

            offset = 0
            if given_name is not None:
                offset = 1

            person_name.insert(offset, surname_element)

        person_name.remove(name)

        # we store the locator last so that the modified version is used
        names_data[xml_file].append(
            (name.text, etree.tostring(person_name), doi, elements_count)
        )


def repatch(
    json_response: dict,
    aws_connector: AWSConnector,
    instrumentation: Instrumentation = None,
):
    """
    Repatch the JSON response with the previously stored names data
    :param json_response: the JSON response
    :param aws_connector: a CLAWS AWS Connector object
    :param instrumentation: a longsight instrumentation object
    :return: a JSON response
    """
    if instrumentation:
        instrumentation.logger.info("Repatching...")

    if "message-type" not in json_response:
        if instrumentation:
            instrumentation.logger.info("Unknown response from API, skipping")
        return json_response

    if (
        json_response["message-type"] != "work"
        and json_response["message-type"] != "works"
    ):
        if instrumentation:
            instrumentation.logger.info("Not a work message, skipping")
        return json_response

    works = []

    if json_response["message-type"] == "work":
        works.append(json_response["message"])
    else:
        works.append(json_response["message"]["items"])

    process_works(aws_connector, instrumentation, json_response, works)

    result_template = {
        "status": "ok",
        "message-version": "1.0.0",
    }

    if len(works) == 1:
        result_template["message-type"] = "work"
        result_template["message"] = works[0]
        return result_template
    else:
        result_template["message-type"] = "works"
        result_template["items"] = works
        return result_template


def process_works(aws_connector, instrumentation, json_response, works):
    """
    Repatch a work or works
    :param aws_connector: a CLAWS AWS Connector object
    :param instrumentation: a longsight instrumentation object
    :param json_response: the JSON response
    :param works: a list of works
    :return: None (the json_response is mutated in place)
    """
    for work in works:
        patches = load_patches(aws_connector, instrumentation, json_response)

        # confirm that the patches are for the correct timestamp
        # TODO: reinstate this logic. It's currently excluded because
        #  the API is returning bad timestamp data. See, for example, the
        #  different timestamp values at
        #  https://api.crossref.org/works/10.5555/GRFG-ENGF as opposed to
        #  https://api.crossref.org/works/10.5555/GRFG-ENGF/transform/application/vnd.crossref.unixsd+xml
        #  the value in the UNIXSD version is correct and what was submitted
        #  but this does not appear in the JSON version.
        """
        for patch_dict in patches:
            for patch_key, patch_str in patch_dict.items():
                patch = json.loads(patch_str)
                if (
                    "timestamp" in patch
                    and patch["timestamp"] != work["deposited"]["timestamp"]
                ):
                    raise Exception(
                        f"Patch timestamp {patch['timestamp']} does not match "
                        f"message timestamp {work['deposited']['timestamp']}"
                    )
                elif "timestamp" not in patch:
                    instrumentation.logger.warning(
                        f"Could not verify patch timestamp. Not found in patch."
                    )
        """

        # build the changelist and countlist
        remaps = {
            "given_name": "given",
            "surname": "family",
            "affiliations": "affiliation",
        }

        include = [
            "given",
            "family",
            "affiliation",
            "name",
            "sequence",
            "ORCID",
        ]

        ignore = [
            "affiliation",
            "institution",
            "institution_id",
            "authenticated-orcid",
            "ORCID",
        ]

        changelist = []
        countlist = []

        build_changelist(
            changelist, countlist, include, patches, remaps, work, ignore
        )

        # apply the patches to the data
        apply_patches(changelist)

        # verify the counts and write debug information if something goes wrong
        remaps = {"given_name": "given", "surname": "family", "name": "names"}
        verify_counts(countlist, remaps)


def load_patches(aws_connector, instrumentation, json_response):
    """
    Load the patches from the S3 bucket
    :param aws_connector: a CLAWS AWS Connector object
    :param instrumentation: a longsight instrumentation object
    :param json_response: the JSON response
    :return: a list of patches
    """
    doi_md5 = extract_doi(instrumentation, json_response)

    patches = deposit.Depositor.load_patches(
        doi_md5=doi_md5,
        aws_connector=aws_connector,
        instrumentation=instrumentation,
        schema_name="name",
    )
    return patches


def build_changelist(
    changelist, countlist, include, patches, remaps, work, ignores
):
    """
    Build the changelist and countlist for the given patches
    :param changelist: the list of changes to make
    :param countlist: expected counts of specific element types
    :param include: elements to include in matching
    :param patches: a list of patches
    :param remaps: a dictionary of remaps
    :param work: the work
    :param ignores: elements to ignore in matching
    :return: None (the changelist and countlist are mutated in place)
    """
    for data in patches:
        for key, val in data.items():
            data_json = json.loads(val)

            # if given_name or surname are blank, then we need to re-add
            # them or the matcher will not be able to find the person element

            """
            "crossref:name",
            "crossref:surname",
            "crossref:given_name",
            """

            if data_json["element_counts"]["crossref:surname"] == 0:
                ...

            from lxml import etree

            # extract person-groups from the JSON
            for author in work["author"]:
                is_match = deposit.Depositor.compare_json_xml(
                    json_block=author,
                    xml_element=etree.fromstring(data_json["locator"]),
                    optional_include=include,
                    remaps=remaps,
                    ignores=ignores,
                )

                if is_match:
                    changelist.append(
                        {"data": data_json["data"], "patch": author}
                    )

                    countlist.append(
                        {
                            "data": data_json["element_counts"],
                            "patch": author,
                        }
                    )


def apply_patches(changelist):
    """
    Apply the patches to the data
    :param changelist: the changelist generated by build_changelist
    :return: None (the changelist is mutated in place)
    """
    for change in changelist:
        data = change["data"]
        patch = change["patch"]

        if "names" not in patch:
            patch["names"] = []

        patch["names"].append(data)


def verify_counts(countlist, remaps):
    """
    Count JSON elements and make sure we have the right number
    :param countlist: a list of counts
    :param remaps: name remaps
    :return: None (the countlist's patches is mutated in place)
    """
    for count in countlist:
        patch = count["patch"]
        for tag, number in count["data"].items():
            tag = tag.rsplit(":", 1)[-1]
            tag = remaps.get(tag, tag)

            if tag not in patch:
                tag_count = 0
            else:
                if isinstance(patch[tag], list):
                    tag_count = len(patch[tag])
                else:
                    tag_count = 1

            if number == 0 and tag in patch:
                del patch[tag]
            elif number != tag_count:
                if "tag_mismatches" not in patch:
                    patch["tag_mismatches"] = []
                patch["tag_mismatches"].append(tag)


def extract_doi(instrumentation, json_response):
    """
    Extract the DOI from the JSON response
    :param instrumentation: a longsight instrumentation object
    :param json_response: the JSON response
    :return: the MD5 of the DOI
    """
    from plugins.annotator import Annotator

    doi = json_response["message"]["DOI"]
    doi_md5 = Annotator.doi_to_md5(doi)
    if instrumentation:
        instrumentation.logger.info(
            f"Processing work message for {doi} ({doi_md5})"
        )
    return doi_md5
