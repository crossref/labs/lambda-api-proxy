import json

import httpx
from claws import aws_utils
from claws.aws_utils import AWSConnector
from fastapi import HTTPException
from httpx import Response
from longsight.instrumentation import Instrumentation
from starlette.datastructures import UploadFile
from starlette.requests import Request


class Depositor:
    def __init__(
        self,
        settings,
        aws_connector: aws_utils.AWSConnector,
        instrumentation: Instrumentation = None,
    ):
        self._aws_connector = aws_connector
        self._settings = settings
        self._instrumentation = instrumentation

    @property
    def instrumentation(self) -> Instrumentation:
        """
        The current instrumentation object.
        :return: an Instrumentation object
        """
        return self._instrumentation

    def process(
        self, xml, schema_modification, xml_filename
    ) -> tuple[dict, dict]:
        """
        Call the rule file for the deposit (depositor_rules/process_{schema_modification}.py)
        :param xml: an XML etree
        :param schema_modification: the schema modification to load
        :param xml_filename: the filename of the XML
        :return: a tuple of the substitution data and the mutated XML
        """
        # import a module called "process_{schema_modification}" from
        # the "depositor_rules" package
        from importlib import import_module

        process_module = import_module(
            f"plugins.depositor_rules.process_{schema_modification}"
        )

        return process_module.process(
            xml=xml,
            xml_filename=xml_filename,
            change_xml_namespace_callable=self.change_xml_namespace,
            validate_xml_callable=self.validate_xml,
        )

    @staticmethod
    def change_xml_namespace(xml, new_schema, version, original_ns):
        from lxml import etree

        xslt = etree.parse(f"plugins/depositor_rules/change_namespace.xsl")

        nsmap = {
            None: new_schema,
            "jats": "http://www.ncbi.nlm.nih.gov/JATS1",
            "fr": "http://www.crossref.org/fundref.xsd",
            "mml": "http://www.w3.org/1998/Math/MathML",
        }

        attr_qname = etree.QName(
            "http://www.w3.org/2001/XMLSchema-instance", "schemaLocation"
        )

        new_tree = xml.xslt(
            xslt,
            orig_namespace=f"'{original_ns}'",
            new_namespace=f"'{new_schema}'",
        )

        root = etree.Element(
            "doi_batch", {attr_qname: new_schema}, version="5.3.1", nsmap=nsmap
        )
        root[:] = new_tree.getroot()

        new_tree = etree.ElementTree(root)

        root = new_tree.getroot()

        root.attrib[
            "{http://www.w3.org/2001/XMLSchema-instance}schemaLocation"
        ] = new_schema

        root.attrib["version"] = version

        return new_tree

    @staticmethod
    def determine_successful_deposits(xml_response) -> list[str]:
        """
        Read back the XML response and return a list of DOIs that have been processed successfully.
        :param xml_response: the XML response from the live API.
        :return: a list of DOIs that have been processed successfully.
        """
        valid_statuses = {"Success", "Warning"}
        try:
            from lxml import etree

            parser = etree.XMLParser(recover=True)
            root = etree.fromstring(xml_response, parser=parser)

            dois = []
            for record_diagnostic in root.findall(".//record_diagnostic"):
                status = record_diagnostic.get("status")

                doi_element = (
                    record_diagnostic.find("doi")
                    if status in valid_statuses
                    else None
                )
                if doi_element is not None:
                    dois.append(doi_element.text)

            return dois
        except etree.XMLSyntaxError:
            return []

    @staticmethod
    def validate_xml(
        xsd_file, xml_element_tree, base_url="", reload=False
    ) -> bool:
        """
        Validate an XML element tree against an XSD file.
        :param xsd_file: the XSD file to validate against
        :param xml_element_tree: the XML element tree to validate
        :param base_url: the base URL of the XSD file against which to resolve relative paths and references
        :param reload: whether to reload/reparse the XML element tree
        :return: True if the XML element tree is valid, False otherwise
        """
        from lxml import etree

        if base_url:
            xsd_tree = etree.parse(
                xsd_file, base_url=base_url if base_url else None
            )
        else:
            xsd_tree = etree.parse(xsd_file)

        if reload:
            # reparse
            xml_element_tree = etree.fromstring(
                etree.tostring(xml_element_tree)
            )

        xml_schema = etree.XMLSchema(xsd_tree)
        return xml_schema.validate(xml_element_tree)

    @staticmethod
    def compare_json_xml(
        json_block,
        xml_element,
        include=None,
        remaps=None,
        optional_include=None,
        ignores=None,
    ) -> bool:
        """
        Compare a JSON block to an XML element.
        :param json_block: the JSON block to compare
        :param xml_element: the XML element to compare
        :param include: a list of fields to include in the comparison (otherwise: all)
        :param remaps: a dictionary of remaps to apply to the JSON block (e.g. "foo" in the JSON corresponds to "bar" in the XML)
        :param optional_include: a list of optional includes that may occur in only the XML or only the JSON, but if in both must be equal
        :param ignores: a list of fields to ignore in the comparison
        :return: True if the block and element match, False otherwise
        """

        def compare_elements(
            json_obj, xml_obj, include, remaps, optional_include, ignores
        ) -> bool:
            from lxml import etree

            if isinstance(json_obj, dict):
                for key in json_obj:
                    # remap the JSON key to natch the XML key
                    json_key = get_remaps(key)

                    # check whether the key should be processed
                    if is_excluded(json_key):
                        continue

                    # remove namespacing from XML keys (so we can use bare tag
                    # names instead of {namespace}tag)
                    found, key, tag = remove_namespace(json_key, key, xml_obj)

                    # the key is not found in the XML
                    if not found:
                        # we can continue the evaluation if the key is in the
                        # optional_includes or ignore list. Otherwise, it's
                        # a failed match.
                        if continue_if_not_found(json_key):
                            continue
                        else:
                            return False

                    # finally, if the key is not in the ignores list, try a
                    # recursive call to this function to compare the tags
                    elif not (
                        ignores and json_key in ignores
                    ) and not compare_elements(
                        json_obj[tag],
                        xml_obj[key],
                        include,
                        remaps,
                        optional_include,
                        ignores,
                    ):
                        return False

                # now work in reverse and check everything in the XML exists
                # in the JSON, too
                for key in xml_obj:
                    # remap the key
                    key = get_remaps(etree.QName(key).localname)

                    # check if the XML tag is in the JSON
                    if ignores and key in ignores:
                        return True
                    elif key not in json_obj and (
                        not optional_include or key not in optional_include
                    ):
                        return False

            # process lists
            elif isinstance(json_obj, list):
                # determine whether this is a length mismatch
                match, ret = is_length_match(xml_obj)

                if not match:
                    return False
                elif match and ret:
                    return True

                for idx, item in enumerate(json_obj):
                    if not compare_elements(
                        item,
                        xml_obj[idx],
                        include,
                        remaps,
                        optional_include,
                        ignores,
                    ):
                        return False
            else:
                # check the actual values match
                return str(json_obj) == xml_obj
            return True

        def is_length_match(xml_obj) -> tuple[bool, bool]:
            """
            Check whether the length of the JSON and XML objects match.
            :param xml_obj: the XML object to check
            :return: a tuple of bools. The first indicates whether the lengths match, the second indicates whether the element is in the ignore list.
            """
            from lxml import etree

            if len(json_obj) != len(xml_obj):
                if (
                    ignores
                    and etree.QName(list(xml_obj.keys())[0]).localname
                    in ignores
                ):
                    return True, True
                return False, False
            return True, False

        def continue_if_not_found(json_key) -> bool:
            """
            Check whether we should continue processing when this JSON key is not found.
            :param json_key: the JSON key
            :return: True if we should continue processing (item is in ignore list or optional include list), False otherwise
            """
            if optional_include and json_key in optional_include:
                return True
            elif ignores and json_key in ignores:
                return True
            else:
                return False

        def is_excluded(json_key) -> bool:
            """
            Check whether the JSON key should be excluded from processing.
            :param json_key: the JSON key
            :return: True if the key should be excluded, False otherwise
            """
            if include and json_key not in include:
                if json_key not in optional_include:
                    return True

            return False

        def get_remaps(field) -> str:
            """
            Get the remapped field name.
            :param field: the field to remap
            :return: the remapped field name
            """
            return remaps.get(field, field)

        def remove_namespace(json_key, key, xml_obj) -> tuple[bool, str, str]:
            """
            De-namespace and remap the XML key
            :param json_key: the JSON key
            :param key: the key to de-namespace in the XML
            :param xml_obj: the XML object
            :return: a tuple of a bool (whether the key was found), the key, and the JSON tag
            """
            from lxml import etree

            found = False
            tag = ""
            for key, val in xml_obj.items():
                tag = get_remaps(etree.QName(key).localname)
                if tag == json_key:
                    found = True
                    break
            return found, key, tag

        def xml_to_dict(xml_element, remaps) -> dict:
            """
            Convert an XML element to a dictionary/JSON version.
            :param xml_element: the XML element
            :param remaps: a dictionary of field remaps
            :return: a dictionary/JSON version of the XML element
            """
            result = {}
            for child in xml_element:
                key = remaps.get(child.tag, child.tag)
                if child:
                    # TODO: The behavior of this check will change in future
                    #  versions. Use specific 'len(elem)' or 'elem is not None'
                    #  test instead. However, at present, "is not None" does not
                    #  work as expected.

                    value = xml_to_dict(child, remaps)
                else:
                    value = child.text
                result[key] = value
            return result

        def remap_keys(d, remaps) -> dict:
            """
            Remap keys in the conversion
            :param d: the dictionary
            :param remaps: the dictionary of remaps
            :return: a new dictionary
            """
            new_d = {}
            for k, v in d.items():
                new_key = remaps.get(k, k)
                if isinstance(v, dict):
                    new_d[new_key] = remap_keys(v, remaps)
                else:
                    new_d[new_key] = v
            return new_d

        if include is None:
            include = []
        if remaps is None:
            remaps = {}
        if optional_include is None:
            optional_include = []

        if isinstance(json_block, str):
            json_obj = json.loads(json_block)
        else:
            json_obj = json_block

        json_obj = remap_keys(json_obj, remaps)
        xml_dict = xml_to_dict(xml_element, remaps)

        return compare_elements(
            json_obj, xml_dict, include, remaps, optional_include, ignores
        )

    def proxy_deposit(
        self, request: Request, files, login_id, login_passwd
    ) -> Response:
        """
        Send a deposit to the live API
        :param request: the request object
        :param files: the files to deposit
        :param login_id: the login credential
        :param login_passwd: the login password
        :return: httpx Response object
        """
        client = request.app.state.client
        url = httpx.URL(
            path="deposits", query=request.url.query.encode("utf-8")
        )

        mut_headers = request.headers.mutablecopy()
        del mut_headers["content-length"]

        data = {
            "login_id": login_id,
            "login_passwd": login_passwd,
            "operation": "doMDUpload",
        }

        req = client.build_request(
            request.method,
            url,
            headers=mut_headers.raw,
            data=data,
            files=files,
        )

        r = client.send(req, stream=True)

        return r

    @staticmethod
    def get_schema_index_key(doi) -> str:
        # md5 the doi
        from plugins import annotator

        doi_hash = annotator.Annotator.doi_to_md5(doi)

        return f"schema-modifications/index/works/{doi_hash}/current.json"

    def store_schema_record(self, doi, schema_name) -> None:
        key = self.get_schema_index_key(doi)

        formatted_data = {
            "doi": doi,
            "schema_name": schema_name,
        }

        if self.instrumentation:
            self.instrumentation.logger.info("Storing schema record/index")

        self._aws_connector.push_json_to_s3(
            json_obj=formatted_data,
            bucket=self._settings.BUCKET,
            path=key,
        )

    def get_known_schema(self, doi):
        key = self.get_schema_index_key(doi)

        index_entry = self._aws_connector.s3_obj_to_str(
            bucket=self._settings.BUCKET, s3_path=key, raise_on_fail=False
        )

        if index_entry is not None:
            try:
                return json.loads(index_entry)["schema_name"]
            except KeyError:
                return None

    def store(
        self, data, locator, doi, schema_name, element_counts, timestamp
    ) -> None:
        """
        Store the data in S3
        :param data: the data to store
        :param locator: the locator of the data (XML element)
        :param doi: the DOI to which this corresponds
        :param schema_name: the schema name
        :return: None
        """
        # store the data in s3://bucket/schema-modifications/<SCHEMA_NAME>>/works/<HASH_OF_DOI>/<HASH_OF_XML_LOOKUP_FRAGMENT>.json

        locator_decoded = locator.decode(encoding="utf-8")

        # md5 the doi and locator
        from plugins import annotator

        doi_hash = annotator.Annotator.doi_to_md5(doi)
        locator_hash = annotator.Annotator.doi_to_md5(locator_decoded)

        key = (
            f"schema-modifications/{schema_name}/works/"
            f"{doi_hash}/{locator_hash}.json"
        )

        formatted_data = {
            "data": data,
            "locator": locator_decoded,
            "doi": doi,
            "element_counts": element_counts,
            "timestamp": timestamp,
        }

        self._aws_connector.push_json_to_s3(
            json_obj=formatted_data,
            bucket=self._settings.BUCKET,
            path=key,
        )

    @staticmethod
    def extract_namespace(
        tree, namespace: str = None, namespace_mode=False
    ) -> str:
        """
        Extract the namespace from an XML tree.
        :param tree: the XML tree
        :param namespace: the namespace or None
        :param namespace_mode: whether to return the namespace or the URL
        :return: the namespace
        """

        if namespace is None:
            return tree.xpath(f"namespace-uri(.)")

        else:
            ns_select = (
                "//namespace::*[not(. = "
                "'http://www.w3.org/XML/1998/namespace') "
                "and . = namespace-uri(..)]"
            )

            for ns, url in tree.xpath(ns_select):
                if not namespace_mode:
                    if ns == namespace:
                        return url
                else:
                    if url == namespace:
                        return ns

    def extract_timestamp(self, xml_document) -> str:
        """
        Extract the timestamp from the XML document
        :param xml_document: the XML document
        :return: the timestamp
        """
        namespace_map = {"ns": self.extract_namespace(xml_document)}
        timestamp = xml_document.xpath(
            "//ns:timestamp/text()", namespaces=namespace_map
        )
        return timestamp[0] if timestamp else None

    async def validate_request(self, request) -> tuple[UploadFile, str, str]:
        """
        Check that this is a valid deposit request and extract the needed info
        :param request: the current request
        :return: a tuple of the submitted file, login_id, and login_passwd
        """
        # 1. check that the content type of this request is "multipart/form-data"
        # 2. check that the following parameters are all set:
        # a. login_id, the depositing user name and/or role in format user/role or role
        # b. login_passwd, the depositing user password
        # c. operation, the value should be “doMDUpload”.
        # 3. extract the file from the request (in the "files" parameter)

        content_type = request.headers.get("content-type")
        if content_type.startswith("multipart/form-data"):
            form = await request.form()

            if (
                "login_id" in form
                and "login_passwd" in form
                and "operation" in form
            ):
                if form["operation"] == "doMDUpload":
                    if "mdFile" in form:
                        return (
                            form["mdFile"],
                            form["login_id"],
                            form["login_passwd"],
                        )
                    else:
                        raise HTTPException(
                            status_code=400,
                            detail="Missing 'mdFile' field in the request.",
                        )
                else:
                    raise HTTPException(
                        status_code=400,
                        detail="Invalid value for 'operation'. "
                        "Must be 'doMDUpload'.",
                    )
            else:
                raise HTTPException(
                    status_code=400,
                    detail="Missing required parameters "
                    "(login_id, login_passwd, operation).",
                )
        else:
            raise HTTPException(
                status_code=400,
                detail="Invalid content type. Must be 'multipart/form-data'.",
            )

    @staticmethod
    def load_patches(
        doi_md5: str,
        aws_connector: AWSConnector,
        instrumentation: Instrumentation = None,
        schema_name="",
    ) -> list[dict]:
        """
        Load the patches in parallel from S3
        :param doi_md5: the MD5 hash of the DOI
        :param aws_connector: a CLAWS AWSConnector object
        :param instrumentation: a longsight Instrumentation object
        :param schema_name: the schema we are working on
        :return: a list[dict] of patches
        """
        if instrumentation:
            instrumentation.logger.info("Loading patches")

        prefix = f"schema-modifications/{schema_name}/works/{doi_md5}/"

        object_list = aws_connector.list_prefix(
            prefix=prefix, file_extension=".json"
        )

        object_list = [prefix + s for s in object_list]

        if instrumentation:
            instrumentation.logger.info("Async loading objects...")

        objects = aws_connector.get_multiple_s3_objs(
            bucket=aws_connector._bucket_name, s3_objs=object_list
        )

        if instrumentation:
            instrumentation.logger.info("Loaded objects...")

        return objects
