from collections import OrderedDict

BUCKET = "outputs.research.crossref.org"
SAMPLES_BUCKET = "samples.research.crossref.org"
ANNOTATION_PATH = "annotations"
VALID_ANNOTATIONS = "annotations/valid_annotations"
REPORTS_PATH = "reports/overall"

IDENTIFIER_KEYS = {
    "works": "DOI",
    "members": "id",
    "journals": "ISSN",
    "types": "id",
    "funders": "id",
}

DATA_PLUGINS = OrderedDict(
    {
        "plugins.crossrefproxy": ("crossrefproxy", "CrossrefProxyPlugin"),
    }
)
PLUGINS = OrderedDict({"plugins.annotator": ("annotator", "Annotator")})

LABS_PREFIX = "cr-labs-"

POLITE_ONLY = True

APP_NAME = "Crossref Labs API"
ABOUT = "The Crossref Labs API"
VERSION = "3.4.4"
MAILTO = "labs@crossref.org"
LICENSE_INFO = {
    "name": "MIT License",
    "url": "https://opensource.org/licenses/MIT",
}
TERMS_OF_SERVICE = """
    Warnings, Caveats and Weasel Words:

    This is an experiment running on R&D equipment in a non-production environment.

    It may disappear without warning and/or perform erratically.

    If it isn’t working for some reason, come back later and try again.
"""

SCHEME = "https"
REPRESENTATION_VERSION = "1.0.0"

HEADERS = {"User-Agent": "f{APP_NAME}; mailto:{MAILTO}"}

MEMBER_ID_TILE = "The member ID"
MEMBER_ID_DESCRIPTION = "The member ID as obtained from the REST API"

ISO_DATE_TITLE = "The date of the sample"
ISO_DATE_DESCRIPTION = "The date of the sample in ISO format (YYYY-MM-DD)"

SAMPLE_LIST_TYPE = "sample-uri-list"
DATA_POINTS_LIST_TYPE = "data-points-list"

TYPE_LIST_TYPE = "type-list"
TYPE_SINGLTON_TYPE = "type"

MEMBER_LIST_TYPE = "member-list"
MEMBER_SINGLTON_TYPE = "member"

WORK_LIST_TYPE = "work-list"
WORK_SINGLTON_TYPE = "work"


RELATIONSHIP_LIST_TYPE = "relationship-list"

LOG_STREAM_NAME = "fargate-API-request"
LOG_STREAM_GROUP = "fargate-API"

SCHEMA_MODIFICATIONS = {
    "http://www.crossref.org/schema/5.4.d0.ror": "ror",
    "http://www.crossref.org/schema/5.4.d0.name": "name",
}

import json
from collections import OrderedDict

import claws.aws_utils


class Settings:
    # Settings for OpCit Deposit System
    BUCKET = "outputs.research.crossref.org"
    SAMPLES_BUCKET = "samples.research.crossref.org"

    APP_NAME = "Op Cit Deposit System"
    ABOUT = "A digital preservation deposit system for Crossref's Labs API"
    VERSION = "0.0.1"
    MAILTO = "labs@crossref.org"
    LICENSE_INFO = {
        "name": "MIT License",
        "url": "https://opensource.org/licenses/MIT",
    }
    TERMS_OF_SERVICE = """
        Warnings, Caveats and Weasel Words:

        This is an experiment running on R&D equipment in a non-production environment.

        It may disappear without warning and/or perform erratically.

        If it isn’t working for some reason, come back later and try again.
    """

    SCHEME = "https"
    REPRESENTATION_VERSION = "1.0.0"

    HEADERS = {"User-Agent": "f{APP_NAME}; mailto:{MAILTO}"}

    LOG_STREAM_NAME = "op-cit-deposit"
    LOG_STREAM_GROUP = "op-cit"

    DEPOSIT_SYSTEMS = {"Internet Archive": "ia"}

    AWS_CONNECTOR = None
    ACCESS_KEY = {}

    DEBUG = False

    @staticmethod
    def fetch_secrets():
        if Settings.ACCESS_KEY == {}:
            Settings.AWS_CONNECTOR = claws.aws_utils.AWSConnector(
                unsigned=False, bucket=Settings.BUCKET, region_name="us-east-1"
            )

            Settings.ACCESS_KEY = json.loads(
                Settings.AWS_CONNECTOR.get_secret("InternetArchiveS3LikeAPI")
            )
