from typing import Union

import settings
from fastapi import APIRouter
from fastapi_proxy import LabsFastAPIProxy
from longsight.instrumentation import instrument
from starlette.requests import Request


router = APIRouter()


@router.get("/{resource_name}/", tags=["proxy and annotate"])
@router.get("/{resource_name}/{identifier}", tags=["proxy and annotate"])
@router.get(
    "/{resource_name}/{identifier}/{other:path}", tags=["proxy and annotate"]
)
@instrument(
    bucket=settings.BUCKET,
    create_aws=True,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
def proxy_and_annotate(
    request: Request,
    resource_name: str = None,
    identifier: str = None,
    other: str = None,
    mailto: Union[str, None] = None,
    plain: Union[str, None] = None,
    instrumentation=None,
):
    # determine whether we need to repatch the response
    schema_name = None

    if resource_name == "works":
        from plugins.deposit import Depositor

        depositor = Depositor(
            aws_connector=instrumentation.aws_connector,
            settings=settings,
            instrumentation=instrumentation,
        )

        schema_name = depositor.get_known_schema(doi=f"{identifier}/{other}")

    proxy = LabsFastAPIProxy(
        settings=settings,
        request=request,
        identifier_key=resource_name,
        aws_connector=instrumentation.aws_connector,
        instrumentation=instrumentation,
        mailto=mailto,
    )

    fetched = proxy.run()

    if schema_name is not None and not (
        plain is not None and not isinstance(plain, dict)
    ):
        if instrumentation:
            instrumentation.logger.info(
                f"Repatching according to schema '{schema_name}'"
            )

        # import a module called "process_{schema_modification}" from
        # the "depositor_rules" package
        from importlib import import_module

        process_module = import_module(
            f"plugins.depositor_rules.process_{schema_name}"
        )

        patched = process_module.repatch(
            json_response=fetched,
            instrumentation=instrumentation,
            aws_connector=instrumentation.aws_connector,
        )

        return patched
    else:
        if instrumentation:
            instrumentation.logger.info(f"No schema modifications to repatch")

    return fetched
