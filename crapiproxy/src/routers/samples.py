from datetime import date

import settings
from fastapi import APIRouter, Path, Request, status
from fastapi.responses import JSONResponse, RedirectResponse
from longsight.instrumentation import instrument
from plugins.sampler import Sampler
from settings import (
    DATA_POINTS_LIST_TYPE,
    ISO_DATE_DESCRIPTION,
    ISO_DATE_TITLE,
    MEMBER_ID_DESCRIPTION,
    MEMBER_ID_TILE,
    SAMPLE_LIST_TYPE,
    SCHEME,
)

router = APIRouter()


@router.get("/works/samples", include_in_schema=False)
@router.get("/works/samples/", tags=["samples"])
@instrument(
    bucket=settings.SAMPLES_BUCKET,
    create_aws=True,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
def get_all_member_samples_available(request: Request, instrumentation=None):
    import settings

    sampling = Sampler(
        aws_connector=instrumentation.aws_connector, settings=settings
    )

    return JSONResponse(
        sampling.response_header("ok", SAMPLE_LIST_TYPE)
        | sampling.message(
            [
                f"{SCHEME}://{sampling.host_name(request)}/works/samples/{d}/"
                for d in sampling.all_member_samples_available()
            ]
        ),
        status_code=status.HTTP_200_OK,
        media_type="application/json",
    )


@router.get("/works/samples/latest", include_in_schema=False)
@router.get("/works/samples/latest/", tags=["samples"])
@instrument(
    bucket=settings.SAMPLES_BUCKET,
    create_aws=True,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
def get_latest_all_member_sample(request: Request, instrumentation=None):
    sampling = Sampler(
        aws_connector=instrumentation.aws_connector, settings=settings
    )

    return RedirectResponse(
        f"/works/samples/{sampling.all_member_samples_available()[-1]}/",
        status_code=status.HTTP_302_FOUND,
    )


@router.get("/works/samples/{iso_date}", include_in_schema=False)
@router.get("/works/samples/{iso_date}/", tags=["samples"])
@instrument(
    bucket=settings.SAMPLES_BUCKET,
    create_aws=True,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
def get_all_member_data_points_for_date(
    request: Request,
    iso_date: date = Path(
        ...,
        title=ISO_DATE_TITLE,
        description=ISO_DATE_DESCRIPTION,
    ),
    instrumentation=None,
):
    sampling = Sampler(
        aws_connector=instrumentation.aws_connector, settings=settings
    )

    return JSONResponse(
        content=sampling.response_header("ok", DATA_POINTS_LIST_TYPE)
        | sampling.message(sampling.all_member_works_data_points(iso_date)),
        status_code=status.HTTP_200_OK,
    )


@router.get("/members/{member_id}/works/samples", include_in_schema=False)
@router.get("/members/{member_id}/works/samples/", tags=["samples"])
@instrument(
    bucket=settings.SAMPLES_BUCKET,
    create_aws=True,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
def get_per_member_samples_available(
    request: Request,
    member_id: str = Path(
        ...,
        title=MEMBER_ID_TILE,
        description=MEMBER_ID_DESCRIPTION,
    ),
    instrumentation=None,
):
    sampling = Sampler(
        aws_connector=instrumentation.aws_connector, settings=settings
    )

    return JSONResponse(
        sampling.response_header("ok", SAMPLE_LIST_TYPE)
        | sampling.message(
            [
                f"{SCHEME}://{sampling.host_name(request)}/members/{member_id}"
                f"/works/samples/{iso_date}/"
                for iso_date in sampling.per_member_samples_available(member_id)
            ]
        ),
        status_code=status.HTTP_200_OK,
        media_type="application/json",
    )


@router.get(
    "/members/{member_id}/works/samples/latest", include_in_schema=False
)
@router.get("/members/{member_id}/works/samples/latest/", tags=["samples"])
@instrument(
    bucket=settings.SAMPLES_BUCKET,
    create_aws=True,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
def get_latest_per_member_sample(
    request: Request,
    member_id: str = Path(
        ...,
        title=MEMBER_ID_TILE,
        description=MEMBER_ID_DESCRIPTION,
    ),
    instrumentation=None,
):
    sampling = Sampler(
        aws_connector=instrumentation.aws_connector, settings=settings
    )

    return RedirectResponse(
        f"/members/{member_id}/works/samples/"
        f"{sampling.per_member_samples_available(member_id)[-1]}/",
        status_code=status.HTTP_302_FOUND,
    )


@router.get(
    "/members/{member_id}/works/samples/{iso_date}", include_in_schema=False
)
@router.get("/members/{member_id}/works/samples/{iso_date}/", tags=["samples"])
@instrument(
    bucket=settings.SAMPLES_BUCKET,
    create_aws=True,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
def get_per_member_data_point_for_date(
    request: Request,
    member_id: str = Path(
        title=MEMBER_ID_TILE, description=MEMBER_ID_DESCRIPTION
    ),
    iso_date: date = Path(
        ...,
        title=ISO_DATE_TITLE,
        description=ISO_DATE_DESCRIPTION,
    ),
    instrumentation=None,
):
    sampling = Sampler(
        aws_connector=instrumentation.aws_connector, settings=settings
    )

    return JSONResponse(
        content=sampling.response_header("ok", DATA_POINTS_LIST_TYPE)
        | sampling.message(
            sampling.per_member_works_data_points(member_id, iso_date)
        ),
        status_code=status.HTTP_200_OK,
    )
