import io
import json

import settings
from fastapi import APIRouter, Path, Request
from httpx import Response
from longsight.instrumentation import instrument
from plugins.deposit import Depositor
from starlette.background import BackgroundTask
from starlette.responses import Response as StarletteResponse

router = APIRouter()


@router.post("/deposit/", tags=["deposit"])
@instrument(
    create_aws=True,
    bucket=settings.BUCKET,
    sign_aws_requests=True,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
async def deposit(
    request: Request,
    instrumentation=None,
):
    """
    Run the deposit-side proxy
    :param request: the request object
    :param instrumentation: longsight instrumentation
    :return: a Starlette Response object
    """
    depositor = Depositor(
        aws_connector=instrumentation.aws_connector,
        settings=settings,
        instrumentation=instrumentation,
    )

    # validate the request
    file, login_id, login_password = await depositor.validate_request(request)

    etree, namespace = etree_and_namespace(file)

    schema_modification = settings.SCHEMA_MODIFICATIONS.get(namespace)

    if schema_modification is None:
        if instrumentation:
            instrumentation.logger.info(
                f"Could not map {namespace} to known schema"
            )
        return StarletteResponse(
            content=schema_error(),
            status_code=400,
        )

    if instrumentation:
        instrumentation.logger.info(
            f"Mapped {namespace} to known schema {schema_modification}"
        )

    etree, is_valid, parsed_xml = validate_xml_against_schema(
        etree, depositor, file, schema_modification
    )

    if not is_valid:
        return StarletteResponse(
            content=validation_error(schema_modification),
            status_code=400,
        )

    # extract the timestamp
    timestamp = depositor.extract_timestamp(parsed_xml)

    # process the XML
    substitution_data, mutated_xml = depositor.process(
        parsed_xml,
        schema_modification,
        xml_filename=file.filename,
    )

    # replace the files object in the request
    xml_bytestream = etree.tostring(mutated_xml[file.filename])

    with io.BytesIO(xml_bytestream) as xml_io:
        (
            response,
            response_body,
            successful_deposits,
        ) = submit_and_validate(
            depositor, file, login_id, login_password, request, xml_io
        )

        store_substitution_data(
            depositor,
            instrumentation,
            schema_modification,
            substitution_data,
            successful_deposits,
            timestamp,
        )

        return StarletteResponse(
            content=response_body,
            status_code=response.status_code,
            headers=response.headers,
            background=BackgroundTask(response.close),
        )


def submit_and_validate(
    depositor, file, login_id, login_password, request, xml_io
):
    uf = {"mdFile": (file.filename, xml_io)}

    # submit the XML to the live DOI registration service
    response: Response = depositor.proxy_deposit(
        request=request,
        files=uf,
        login_id=login_id,
        login_passwd=login_password,
    )

    response_body = response.read()

    # determine which deposits succeeded
    successful_deposits = depositor.determine_successful_deposits(
        xml_response=response_body
    )

    return response, response_body, successful_deposits


def etree_and_namespace(file):
    # validate the XML against the schema
    from lxml import etree

    parsed_xml = etree.parse(file.file)

    return parsed_xml, Depositor.extract_namespace(parsed_xml)


def validate_xml_against_schema(
    parsed_xml, depositor, file, schema_modification
):
    from lxml import etree

    # TODO: this must select the right XSD file

    is_valid = depositor.validate_xml(
        xsd_file=f"plugins/depositor_schema/{schema_modification}/crossref5.4.d0.xsd",
        xml_element_tree=parsed_xml,
    )
    return etree, is_valid, parsed_xml


def store_substitution_data(
    depositor,
    instrumentation,
    schema_modification,
    substitution_data,
    successful_deposits,
    timestamp,
):
    # store the return values in S3 if deposit succeeded
    for key, value in substitution_data.items():
        gen = (
            data_point
            for data_point in value
            if data_point[2] in successful_deposits
        )

        store_data_in_s3(
            depositor, gen, instrumentation, schema_modification, timestamp
        )


def store_data_in_s3(
    depositor, gen, instrumentation, schema_modification, timestamp
):
    done_dois = []

    for data_point in gen:
        if data_point[2] not in done_dois:
            delete_existing_objects(
                data_point, instrumentation, schema_modification
            )
            done_dois.append(data_point[2])

        instrumentation.logger.info(f"Storing data for {data_point[2]}")

        depositor.store(
            data=data_point[0],
            locator=data_point[1],
            doi=data_point[2],
            schema_name=schema_modification,
            element_counts=data_point[3],
            timestamp=timestamp,
        )

        # now store a record that ties this DOI to the schema
        depositor.store_schema_record(
            doi=data_point[2], schema_name=schema_modification
        )


def delete_existing_objects(data_point, instrumentation, schema_modification):
    # delete existing records in S3
    from plugins import annotator

    instrumentation.logger.info(f"Deleting files for {data_point[2]}")

    doi_hash = annotator.Annotator.doi_to_md5(data_point[2])
    prefix = f"schema-modifications/{schema_modification}/works/{doi_hash}/"

    deleted = instrumentation.aws_connector.delete_under_prefix(prefix=prefix)

    for file in deleted:
        instrumentation.logger.info(
            f"Deleted {file} from S3 for {data_point[2]}"
        )


def validation_error(schema_name: str):
    """
    Return a Crossref validation error when the schema is invalid
    :param schema_name: the schema name
    :return: an XML string
    """
    response = (
        '<?xml version="1.0" encoding="UTF-8"?>'
        '<doi_batch_diagnostic status="completed" sp="cskAir.local">'
        '<record_diagnostic status="Failure" msg_id="4">'
        f'<msg>Record not processed because submitted XML does not match the new schema for "{schema_name if schema_name.isalnum() else "Invalid Schema"}"</msg>'
        "</record_diagnostic>"
        "<batch_data>"
        "<record_count>1</record_count>"
        "<success_count>0</success_count>"
        "<warning_count>0</warning_count>"
        "<failure_count>1</failure_count>"
        "</batch_data>"
        "</doi_batch_diagnostic>"
    )

    return response


def schema_error():
    """
    Return a Crossref validation error when the schema is not found
    :return: an XML string
    """
    response = (
        '<?xml version="1.0" encoding="UTF-8"?>'
        '<doi_batch_diagnostic status="completed" sp="cskAir.local">'
        '<record_diagnostic status="Failure" msg_id="4">'
        f'<msg>Record not processed because submitted XML does not match any known schema"</msg>'
        "</record_diagnostic>"
        "<batch_data>"
        "<record_count>1</record_count>"
        "<success_count>0</success_count>"
        "<warning_count>0</warning_count>"
        "<failure_count>1</failure_count>"
        "</batch_data>"
        "</doi_batch_diagnostic>"
    )

    return response
