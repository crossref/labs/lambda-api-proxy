import pathlib
import sys

import settings
from fastapi import APIRouter, Request
from longsight.instrumentation import instrument
from starlette.responses import Response as StarletteResponse
import opcit.interstitial
from starlette.templating import Jinja2Templates

router = APIRouter()


@router.post("/deposit/preserve/", tags=["deposit", "preservation"])
@instrument(
    create_aws=True,
    bucket=settings.BUCKET,
    sign_aws_requests=True,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
def deposit(
    request: Request,
    instrumentation=None,
):
    return StarletteResponse(
        content="Welcome to Op Cit",
        status_code=200,
        headers={},
    )


@router.get("/opcit/{identifier}", tags=["preservation"])
@instrument(
    create_aws=True,
    bucket=settings.Settings.BUCKET,
    sign_aws_requests=True,
    cloudwatch_push=True,
    log_group_name=settings.Settings.LOG_STREAM_GROUP,
    log_stream_name=settings.Settings.LOG_STREAM_NAME,
)
async def interstitial(
    request: Request,
    instrumentation=None,
    identifier: str = None,
):
    path = pathlib.Path("/app/app")

    if path.exists():
        sys.path.append("/app/app")
        sys.path.append("/app/app/templates")

    if path.exists():
        templates = Jinja2Templates(directory="/app/app/templates")
    else:
        templates = Jinja2Templates(directory="templates")

    return await opcit.interstitial.Interstitial(
        request=request, instrumentation=instrumentation
    ).run(identifier, templates=templates)
