import io
from datetime import date, datetime, timedelta

import settings
from fastapi import APIRouter, Path, Request
from fastapi.responses import JSONResponse, RedirectResponse
from longsight.instrumentation import instrument
from plugins.sampler import Sampler
from starlette.responses import StreamingResponse

router = APIRouter()


@router.get("/data/retractionwatch", response_class=StreamingResponse)
@router.get("/dumps/retractions", response_class=StreamingResponse)
@instrument(
    bucket=settings.SAMPLES_BUCKET,
    create_aws=True,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
def get_rw_dump(request: Request, instrumentation=None):
    stream = get_retraction_watch_stream()
    response = StreamingResponse(content=stream.iter_chunks(1024 * 20))

    response.headers[
        "Content-Disposition"
    ] = "attachment; filename=retractions.csv"
    return response


def get_retraction_watch() -> str | None:
    """
    Download the retraction watch dump from S3
    :return: CSV string
    """

    from claws import aws_utils

    current_date = datetime.now()

    aws_connector = aws_utils.AWSConnector(
        bucket="org.crossref.research.retractionwatch", unsigned=False
    )

    # look back over the past 15 days
    for x in range(1, 15):
        try:
            s3_path = f"uploads/RWDBDNLD{current_date.strftime('%Y%m%d')}.csv"

            print(f"Trying {s3_path}")

            csv = aws_connector.s3_client.get_object(
                Bucket="org.crossref.research.retractionwatch",
                Key=s3_path,
            )["Body"].read()

            csv_str = csv.decode("latin1")

            print(f"Loaded {s3_path}")
            return csv_str
        except:
            # take one day off current date
            current_date = current_date - timedelta(days=1)

    print("No data file found")


def get_retraction_watch_stream():
    """
    Download the retraction watch dump from S3
    :return: CSV string
    """

    from claws import aws_utils

    current_date = datetime.now()

    aws_connector = aws_utils.AWSConnector(
        bucket="org.crossref.research.retractionwatch", unsigned=False
    )

    # look back over the past 15 days
    for x in range(1, 15):
        try:
            s3_path = f"uploads/RWDBDNLD{current_date.strftime('%Y%m%d')}.csv"

            print(f"Trying {s3_path}")

            csv = aws_connector.s3_client.get_object(
                Bucket="org.crossref.research.retractionwatch",
                Key=s3_path,
            )["Body"]

            print(f"Loaded {s3_path}")
            return csv
        except:
            try:
                s3_path = (
                    f"uploads/RWDBDNLD{current_date.strftime('%Y%m0%d')}.csv"
                )

                print(f"Trying {s3_path}")

                csv = aws_connector.s3_client.get_object(
                    Bucket="org.crossref.research.retractionwatch",
                    Key=s3_path,
                )["Body"]

                print(f"Loaded {s3_path}")
                return csv

            except:
                # take one day off current date
                current_date = current_date - timedelta(days=1)

    print("No data file found")
    return None
