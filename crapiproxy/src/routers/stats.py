import json
from typing import Union

import settings
import whatismyip
from fastapi import APIRouter, Request
from longsight.instrumentation import instrument
from pathlib import Path

router = APIRouter()


@router.get("/stats/", tags=["stats"])
@instrument()
def show_stats(
    request: Request,
    resource_name: str = None,
    identifier: str = None,
    other: str = None,
    mailto: Union[str, None] = None,
    instrumentation=None,
):
    return {
        "ipv4": whatismyip.whatismyipv4(),
        "ipv6": whatismyip.whatismyipv6(),
    }


@router.get("/reports/", tags=["reports"])
@instrument(
    create_aws=True,
    bucket=settings.BUCKET,
    cloudwatch_push=True,
    log_group_name=settings.LOG_STREAM_GROUP,
    log_stream_name=settings.LOG_STREAM_NAME,
)
def show_reports(
    request: Request,
    instrumentation=None,
):
    aws_connector = instrumentation.aws_connector

    json_files = aws_connector.list_prefix(
        prefix=settings.REPORTS_PATH, file_extension=".json"
    )

    result_template = {
        "status": "ok",
        "message-type": "report",
        "message-version": "1.0.0",
        "message": {},
    }

    for json_file in json_files:
        unstemmed_path = Path(json_file).stem
        name_to_use = f"{settings.LABS_PREFIX}{unstemmed_path}"

        file_contents = aws_connector.s3_obj_to_str(
            bucket=settings.BUCKET,
            s3_path=f"{settings.REPORTS_PATH}/{json_file}",
        )

        result_template["message"][name_to_use] = json.loads(file_contents)

    return result_template
