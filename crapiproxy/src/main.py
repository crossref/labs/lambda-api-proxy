import pathlib
import sys


import sentry_sdk
from starlette.responses import Response

path = pathlib.Path("/app/app")

if path.exists():
    sys.path.append("/app/app")
    sys.path.append("/app/app/templates")


import httpx
import longsight.instrumentation as instrumentation
from fastapi import FastAPI, Request

from starlette import status
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates

from routers import annotation, stats, samples, deposit, dumps, opcit
from settings import (
    ABOUT,
    APP_NAME,
    LICENSE_INFO,
    MAILTO,
    TERMS_OF_SERVICE,
    VERSION,
)

sentry_sdk.init(
    dsn="https://2c610cef58cbefc9f4a841d5636e7a61@o4506575632203776.ingest.sentry.io/4506575634563072",
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    traces_sample_rate=1.0,
    # Set profiles_sample_rate to 1.0 to profile 100%
    # of sampled transactions.
    # We recommend adjusting this value in production.
    profiles_sample_rate=1.0,
)

app = FastAPI(
    description=ABOUT,
    title=APP_NAME,
    version=VERSION,
    contact={"email": MAILTO},
    license_info=LICENSE_INFO,
    terms_of_service=TERMS_OF_SERVICE,
)

try:
    app.mount("/static", StaticFiles(directory="static"), name="static")
except:
    ...

if path.exists():
    templates = Jinja2Templates(directory="/app/app/templates")
else:
    templates = Jinja2Templates(directory="templates")


@app.on_event("startup")
def startup_event():
    client = httpx.Client(base_url="https://test.crossref.org/v2/", timeout=30)

    # TODO: reinstate the live URL
    """
    client = httpx.AsyncClient(
        base_url="https://doi.crossref.org/v2/", timeout=30
    )
    """
    app.state.client = client


@app.on_event("shutdown")
def shutdown_event():
    client = app.state.client
    client.close()


app.router.route_class = instrumentation.LoggerRouteHandler

# note that the annotation router should be last, as it is the fallback route

app.include_router(stats.router)
app.include_router(opcit.router)
app.include_router(dumps.router)
app.include_router(samples.router)
app.include_router(deposit.router)
app.include_router(annotation.router)


app.add_middleware(instrumentation.AWSCorrelationIdMiddleware)


@app.get("/")
def read_root(request: Request):
    return templates.TemplateResponse(
        "about.html",
        {
            "request": request,
            "app_name": APP_NAME,
            "version": VERSION,
            "about": ABOUT,
            "terms_of_service": TERMS_OF_SERVICE,
        },
        status_code=status.HTTP_200_OK,
    )


@app.get("/heartbeat")
async def heartbeat(request: Request):
    return Response(content="OK", status_code=status.HTTP_200_OK)


@app.get("/sentry-debug")
async def trigger_error():
    division_by_zero = 1 / 0


@app.get("/url-list")
def get_all_urls():
    url_list = [
        {"path": route.path, "name": route.name} for route in app.routes
    ]
    return url_list
