FROM public.ecr.aws/docker/library/python:3.11

WORKDIR /app

COPY ./crapiproxy/src/ /app/app/
COPY ./crapiproxy/src/* /app/app/
COPY ./crapiproxy/requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80", "--loop", "asyncio"]
