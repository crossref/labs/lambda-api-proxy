# Crossref Labs API
The reverse proxy system for the Crossref Labs API. This system allows us to model new metadata schemas.


![license](https://img.shields.io/gitlab/license/crossref/labs/lambda-api-proxy) ![activity](https://img.shields.io/gitlab/last-commit/crossref/labs/lambda-api-proxy)

![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white) ![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![Terraform](https://img.shields.io/badge/terraform-%235835CC.svg?style=for-the-badge&logo=terraform&logoColor=white)

This Python FastAPI implements the Crossref Labs API proxy system. The system aims to provide a bi-directional intercept to the live deposit system, prototyping new metadata schemas for members to test.

This is a prototype Crossref Labs system. It is not guaranteed to be stable and the metadata schema and behaviour may be subject to change at any time.

## Running it Yourself

Infrastructure can be found in [the Crossref infrastructure repository](https://gitlab.com/crossref/infrastructure/terraform), under crossref/research/micro.

## Features
* Proxy for Crossref Labs API with additional data fields. Example: https://api.labs.crossref.org/members/?mailto=your@email.com
* Unpaginated routes (all records). You can use this by specifying a number of rows greater than found in the route or using the value "all". These exist on a 24-hour time delay from the live API. Example: https://api.labs.crossref.org/members/?mailto=your@email.com&rows=10000000
* "Simple" mode on the unpaginated members route, using select. Note that you _cannot_ select arbitrary fields. The only supported combinations are: id and primary-title or id and primary-title and names. These exist on a 24-hour time delay from the live API. Examples: https://api.labs.crossref.org/members/?mailto=your@email.com&rows=10000000&select=id,primary-title or https://api.labs.crossref.org/members/?mailto=your@email.com&rows=10000000&select=id,primary-title,names
* Deposit proxy for Crossref Labs API. See the [deposit proxy documentation](crapiproxy/docs/DEPOSIT.md).

## Tests
Current [build status is available on GitLab Pages](https://crossref.gitlab.io/labs/lambda-api-proxy/).

Tests are currently a hybrid of pytest and unittest.TestCase types.

## Important Notes on Deposit Proxy Version Matching
The deposit proxy offers the ability to model new schemas, repatching the JSON data on return. However, at present there is a bug in the design of the Crossref Live API that prevents us from checking that the data we have stored for patching definitely matches the data in the live system.

Until this is fixed, it is, therefore, possible for the data we have to fall out of sync with the live API. This will happen if a user:

* Initially deposits using the Labs API proxy
* Subsequently updates the metadata using the Live API
* Requests the entry via the Labs API proxy

Example code to show how to resolve this is in the process_works function in process_name.py, but it is currently commented out until the deposit timestamp matches that submitted.

The solution to this is: if you want the Labs API to work, always use Labs API endpoints. If you modify a metadata record in the Live system, do not expect the Labs API to return good data. This is _undefined behaviour_.

# Credits
* [.gitignore](https://github.com/github/gitignore) from Github.
* [Terraform](https://www.terraform.io/) by Hashicorp.

&copy; Crossref 2023