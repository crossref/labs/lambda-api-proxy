# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Since Last Version
* Stubs and initial work for ROR deposit
* Partial work on ROR deposit

## [3.4.1] - 2024-02-22
* Redirect to presigned URL for "all" routes. This is a fix for various performance issues with these routes crashing the API.

## [3.4.0] - 2024-02-15
* Basic Op Cit support. This is a work in progress and is not yet functional.

## [3.3.5] - 2024-01-16
* Several performance fixes, including some to "all" routes

## [3.3.4] - 2024-01-08
* Upgrade clannotation to fix bug in journal annotation

## [3.3.3] - 2023-12-11
* Add heartbeat route

## [3.3.2] - 2023-11-22
* Add opcit dependency

## [3.3.2] - 2023-11-22
* Fix journal annotations

## [3.3.0] - 2023-09-26
* Begin implementation of Op Cit

## [3.2.3] - 2023-09-11
* Update URL for Retraction Watch data

## [3.2.2] - 2023-09-11
* Update URL for Retraction Watch data

## [3.2.1] - 2023-09-06
* Change CSV dump to use latin1 encoding

## [3.2.0] - 2023-09-06
* Added ability to retrieve Retraction Watch CSV at /dumps/retractions 

## [3.1.0] - 2023-07-31
* Fixes to row fetching

## [3.0.0] - 2023-07-28
* Migration to new infrastructure

## [2.9.0] - 2023-07-12
* Fixes to annotation system

## [2.8.1] - 2023-07-12
* Fix to works route when no schema specified

## [2.8.0] - 2023-06-27
* Add ?plain=true querystring option. This will bypass the schema modification repatching.

## [2.7.0] - 2023-06-27
* Move deposit to unified endpoint at /deposit
* Write an index file indicating which schema should be repatched (only one allowed)
* Determine repatching based on index file, thereby unifying the request endpoints

## [2.6.0] - 2023-06-26
* Add new "select" option to simple members route: "name"

## [2.5.1] - 2023-06-22
* Enable the funders route.

## [2.5.0] - 2023-06-22
* Change "simple" querystring to use "select" syntax. This harmonises syntax with the live API.
* Change "rows" querystring to use "all" route when a number that is higher than the total number of records is given. Giving an arbitrarily high value for this will work.

## [2.4.0] - 2023-06-22
* Initial support for content negotiation. At this point, we only support /transform/application/members-all-simple+json and the processing is _not_ generic. However, this PoC works. [RD-161](https://crossref.atlassian.net/browse/RD-161) 

## [2.3.0] - 2023-06-20
* Added simple=True flag to ?rows=all functionality. Fixes [RD-145](https://crossref.atlassian.net/browse/RD-145).

## [2.2.0] - 2023-06-13
* The ?rows=all routes are now delivered by proxying through the system, using a StreamingResponse, rather than by providing a presigned URL. This fixes [RD-135](https://crossref.atlassian.net/browse/RD-135).
* Docker Python version bumped to 3.11 to give speed increase.
* Update CLAWS version to 0.0.20 to improve logging.
* Update Fargate task spec to higher level.
* Update clannotation version to next version.

## [2.1.2] - 2023-06-13
* Fix bug in multi-threaded fetch that misidentified JSON lists as "not found". Fixes [RD-134](https://crossref.atlassian.net/browse/RD-134).
* Update CLAWS version to 0.0.19 to fix bug in multi-threaded fetch.

## [2.1.1] - 2023-06-13
* [Reintroduce async fetch using event loop](https://crossref.atlassian.net/jira/software/c/projects/RD/boards/31?modal=detail&selectedIssue=RD-117).
* Update CLAWS version 0.0.18 to initiate an event loop if one is not running.

## [2.1.0] - 2023-06-13
* [Switch from async routes to sync routes](https://crossref.atlassian.net/browse/RD-133). This prevents blocking across multiple requests.
* Rewrite tests to use sync routes.

## [2.0.2] - 2023-06-12
* Switch schema annotator to use synchronous download.

## [2.0.1] - 2023-06-11
* Fixes to test suite.
* Update logging to push to Cloudwatch.
* Update Terraform execution role policies to allow Cloudwatch logging.

## [2.0.0] - 2023-06-10
* Updated build/deployment scripts.
* Updated README.md to reflect new Fargate system.
* Converted Terraform infrastructure provision to AWS Fargate rather than Lambda functions. This allows us to circumvent payload limits and difficult LXML architecture dependencies.